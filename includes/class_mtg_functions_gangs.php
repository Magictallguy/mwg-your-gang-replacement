<?php
/*Everyone is permitted to copy and distribute verbatim or modified copies of this license document, and changing it is allowed as long as the name is changed.
	DON'T BE A DICK PUBLIC LICENSE TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

	Do whatever you like with the original work, just don't be a dick.
	Being a dick includes - but is not limited to - the following instances:
	1a. Outright copyright infringement - Don't just copy this and change the name.
	1b. Selling the unmodified original with no work done what-so-ever, that's REALLY being a dick.
	1c. Modifying the original work to contain hidden harmful content. That would make you a PROPER dick.

	If you become rich through modifications, related works/services, or supporting the original work, share the love. Only a dick would make loads off this work and not buy the original works creator(s) a pint.
Code is provided with no warranty. Using somebody else's code and bitching when it goes wrong makes you a DONKEY dick. Fix the problem yourself. A non-dick would submit the fix back.*/
if(!defined('MTG_ENABLE'))
	exit('Direct access not permitted');
class mtg_functions_gangs {
	static $inst = null;
	static public function getInstance() {
		if(self::$inst == null)
			self::$inst = new mtg_functions_gangs();
		return self::$inst;
	}
	public function gang_event_add($gang = 0, $event = 'None') {
		global $db, $gangdata;
		if(!$gang)
			return false;
		if($gang != $gangdata['gangID']) {
			$select = $db->query("SELECT `gangID` FROM `gangs` WHERE `gangID` = ".$gang);
			if(!$db->num_rows($select))
				return false;
		}
		$db->query("INSERT INTO `gangevents` (`gevGANG`, `gevTIME`, `gevTEXT`) VALUES (".$gang.", ".time().", '".$db->escape($event)."')");
	}
	public function isLeader($viceToo = false) {
		global $ir, $gangdata;
		$leaders = [$gangdata['gangPRESIDENT']];
		if($viceToo)
			$leaders[] = $gangdata['gangVICEPRES'];
		return in_array($ir['userid'], $leaders) ? true : false;
	}
	public function display($id = 0, $name = null) {
		global $db, $mtg;
		if(!$id)
			return 'Deleted Gang';
		if($name)
			return "<a href='gangs.php?action=view&amp;ID=".$id."'>".$mtg->format($name)."</a>";
		else {
			$select = $db->query("SELECT `gangNAME` FROM `gangs` WHERE `gangID` = ".$id);
			return $db->num_rows($select) ? "<a href='gangs.php?action=view&amp;ID=".$id."'>".$mtg->format($db->fetch_single($select))."</a>" : 'Deleted Gang';
		}
	}
	public function isMember($id = 0) {
		global $db, $gangdata;
		if(!$id)
			return false;
		$selectUser = $db->query("SELECT `gang` FROM `users` WHERE `userid` = ".$id);
		if(!$db->num_rows($selectUser))
			return false;
		return $db->fetch_single($selectUser) == $gangdata['gangID'] ? true : false;
	}
	public function memberCount($id = 0) {
		global $db, $gangdata;
		if(!$id)
			$id = $gangdata['gangID'];
		$selectGang = $db->query("SELECT `gangID` FROM `gangs` WHERE `gangID` = ".$id);
		if(!$db->num_rows($selectGang))
			return 0;
		$selectMembers = $db->query("SELECT COUNT(`userid`) FROM `users` WHERE `gang` = ".$id);
		return $db->fetch_single($selectMembers);
	}
	public function listMembers($id = 0, $name = 'user', $opt0 = false, $selected = -1) {
		global $db, $gangdata, $mtg;
		if(!$id)
			$id = $gangdata['gangID'];
		$ret = "<select name='".$name."'>";
		if($opt0)
			$ret .= "<option value='0'>--- SELECT ---</option>";
		if(!$this->memberCount())
			return $mtg->error("An error has occured, please report to the staff");
		$select = $db->query("SELECT `userid`, `username` FROM `users` WHERE `gang` = ".$id);
		while($row = $db->fetch_row($select))
			$ret .= sprintf("<option value='%u'%s>%s</option>", $row['userid'], $selected && $selected == $row['userid'] ? " selected='selected'" : '', $mtg->format($row['username']));
		$ret .= "</select>";
		return $ret;
	}
}
$gangs = mtg_functions_gangs::getInstance();