<?php
/*Everyone is permitted to copy and distribute verbatim or modified copies of this license document, and changing it is allowed as long as the name is changed.
	DON'T BE A DICK PUBLIC LICENSE TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

	Do whatever you like with the original work, just don't be a dick.
	Being a dick includes - but is not limited to - the following instances:
	1a. Outright copyright infringement - Don't just copy this and change the name.
	1b. Selling the unmodified original with no work done what-so-ever, that's REALLY being a dick.
	1c. Modifying the original work to contain hidden harmful content. That would make you a PROPER dick.

	If you become rich through modifications, related works/services, or supporting the original work, share the love. Only a dick would make loads off this work and not buy the original works creator(s) a pint.
Code is provided with no warranty. Using somebody else's code and bitching when it goes wrong makes you a DONKEY dick. Fix the problem yourself. A non-dick would submit the fix back.*/
if(!defined('MTG_ENABLE'))
	exit('Direct access not permitted');
class mtg_functions {
	static $inst = null;
	static public function getInstance() {
		if(self::$inst == null)
			self::$inst = new mtg_functions();
		return self::$inst;
	}
	public function error($msg) {
		global $h;
		echo '<h3>Error</h3><div class="error">'.$msg.'</div>';
		exit($h->endpage());
	}
	public function success($msg, $kill = false) {
		global $h;
		echo '<h3>Success</h3><div class="success">'.$msg.'</div>';
		if($kill)
			exit($h->endpage());
	}
	public function s($num, $word = '') {
		if(!$word)
			return $num == 1 ? '' : 's';
		else {
			if(!ctype_alpha(substr($word, -1)))
				return null;
			if(substr($word, -2) == 'es')
				return $num == 1 ? '' : '\'';
			else if(substr($word, -1) == 'y')
				return $num == 1 ? 'y' : 'ies';
			else
				return substr($word, -1) == 's' ? '' : ($num == 1 ? '' : 's');
		}
	}
	public function format($str, $dec = 0) {
		$ret = is_numeric($str) ? number_format($str, $dec) : stripslashes(strip_tags($str));
		if(!is_numeric($str) && $dec)
			$ret = nl2br($ret);
		return $ret;
	}
	public function time_format($seconds, $mode = 'long'){
		$names	= [
			'long' => ['millenia', 'year', 'month', 'day', 'hour', 'minute', 'second'],
			'short' => ['mil', 'yr', 'mnth', 'day', 'hr', 'min', 'sec']
		];
		$seconds  = floor($seconds);
		$minutes  = intval($seconds / 60);
		$seconds -= $minutes * 60;
		$hours	= intval($minutes / 60);
		$minutes -= $hours * 60;
		$days	 = intval($hours / 24);
		$hours   -= $days * 24;
		$months   = intval($days / 31);
		$days	-= $months * 31;
		$years	= intval($months / 12);
		$months  -= $years * 12;
		$millenia = intval($years / 1000);
		$years -= $millenia * 1000;
		$result   = [];
		if($millenia)
			$result[] = sprintf("%s %s", $this->format($millenia), $names[$mode][0]);
		if($years)
			$result[] = sprintf("%s %s%s", $this->format($years), $names[$mode][1], $this->s($years));
		if($months)
			$result[] = sprintf("%s %s%s", $this->format($months), $names[$mode][2], $this->s($months));
		if($days)
			$result[] = sprintf("%s %s%s", $this->format($days), $names[$mode][3], $this->s($days));
		if($hours)
			$result[] = sprintf("%s %s%s", $this->format($hours), $names[$mode][4], $this->s($hours));
		if($minutes && count($result) < 2)
			$result[] = sprintf("%s %s%s", $this->format($minutes), $names[$mode][5], $this->s($minutes));
		if(($seconds && count($result) < 2) || !count($result))
			$result[] = sprintf("%s %s%s", $this->format($seconds), $names[$mode][6], $this->s($seconds));
		return implode(", ", $result);
	}
	public function username($id = 0, $showID = false) {
		global $db, $set;
		if(!$id)
			return "<span style='color:#555;font-style:italic;'>System</span>";
		$selectUser = $db->query("SELECT `username`, `donatordays`, `fedjail`, `user_level`, `hospital`, `jail`, `gang`, `forumban`, `mailban` FROM `users` WHERE `userid` = ".$id);
		if(!$db->num_rows($selectUser))
			return "<span style='color:#555;font-style:italic;'>System</span>";
		$user = $db->fetch_row($selectUser);
		$ret = '';
		$user['username'] = $this->format($user['username']);
		$gangShiz = '';
		if($user['gang']) {
			$selectGang = $db->query("SELECT `gangPREF` FROM `gangs` WHERE `gangID` = ".$user['gang']);
			if($db->num_rows($selectGang))
				if($db->fetch_single($selectGang) != '')
					$gangShiz .= '['.substr($this->format($db->fetch_single($selectGang)), 0, 3).'] ';
		}
		if($user['user_level'] > 1) {
			$staffColours = isset($set['staff_colour_enabled']) && $set['staff_colour_enabled']
				? [
					2 => (!isset($set['staff_colour_admin']) || empty($set['staff_colour_admin'])) ? '003EFF' : $set['staff_colour_admin'],
					3 => (!isset($set['staff_colour_secretary']) || empty($set['staff_colour_secretary'])) ? '551A8B' : $set['staff_colour_secretary'],
					5 => (!isset($set['staff_colour_assistant']) || empty($set['staff_colour_assistant'])) ? 'CD6600' : $set['staff_colour_assistant'],
				]
				: [
					2 => '003EFF',
					3 => '551A8B',
					5 => 'CD6600',
				];
			$ret .= "<a href='viewuser.php?u=".$id."' style='color:#".$staffColours[$user['user_level']].";' title='".($user['donatordays'] ? ' - Donator: '.$this->time_format($user['donatordays'] * 86400).' Left' : '')."'>".$gangShiz.$user['username']."</a>";
		} else {
			if($user['donatordays'])
				$ret .= "<a href='viewuser.php?u=".$id."' title='Donator: ".$this->time_format($user['donatordays'] * 86400)." left'>".$gangShiz.$user['username']."</a>";
			else
				$ret .= "<a href='viewuser.php?u=".$id."'>".$gangShiz.$user['username']."</a>";
			if($user['fedjail'])
				$ret = "<a href='viewuser.php?u=".$id."'>".$gangShiz."<span style='text-decoration:strikethrough;color:#444;'>".$user['username']."</span></a>";
		}
		if($showID)
			$ret .= ' ['.$this->format($id).']';
		if($user['mailban'])
			$ret .= " <img src='img/silk/email_delete.png' title='Banned from mail' alt='Banned from mail' />";
		if($user['forumban'])
			$ret .= " <img src='img/silk/user_delete.png' title='Banned from forum' alt='Banned from forum' />";
		if($user['hospital'])
			$ret .= " <img src='img/silk/pill.png' title='Hospitalised' alt='Hospitalised' />";
		if($user['jail'])
			$ret .= " <img src='img/silk/lock.png' title='Jailed' alt='Jailed' />";
		return $ret;
	}
	public function ifExists($id = 0) {
		global $db;
		if(!$id)
			return false;
		$selectUser = $db->query("SELECT `userid` FROM `users` WHERE `userid` = ".$id);
		return $db->num_rows($selectUser) ? true : false;
	}
	public function showLocation($id = 0) {
		global $db;
		if(!$id)
			return false;
		$selectUser = $db->query("SELECT `location` FROM `users` WHERE `userid` = ".$id);
		if(!$db->num_rows($selectUser))
			return false;
		$location = $db->fetch_single($selectUser);
		$selectLocation = $db->query("SELECT `cityname` FROM `cities` WHERE `cityid` = ".$location);
		return $db->num_rows($selectLocation) ? "<a href='monorail.php?to=".$location."'>".$this->format($db->fetch_single($selectLocation))."</a>" : 'Non-existant location';
	}
}
$mtg = mtg_functions::getInstance();