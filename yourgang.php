<?php
/*
Everyone is permitted to copy and distribute verbatim or modified copies of this license document, and changing it is allowed as long as the name is changed.
	DON'T BE A DICK PUBLIC LICENSE TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

	Do whatever you like with the original work, just don't be a dick.
	Being a dick includes - but is not limited to - the following instances:
	1a. Outright copyright infringement - Don't just copy this and change the name.
	1b. Selling the unmodified original with no work done what-so-ever, that's REALLY being a dick.
	1c. Modifying the original work to contain hidden harmful content. That would make you a PROPER dick.

	If you become rich through modifications, related works/services, or supporting the original work, share the love. Only a dick would make loads off this work and not buy the original works creator(s) a pint.
Code is provided with no warranty. Using somebody else's code and bitching when it goes wrong makes you a DONKEY dick. Fix the problem yourself. A non-dick would submit the fix back.
*/
require_once(__DIR__.'/globals.php');
if(!defined('MTG_ENABLE'))
	define('MTG_ENABLE', true);
require_once(__DIR__ . '/includes/class_mtg_functions.php');
if(!$ir['gang'])
	$mtg->error("You're not in a Gang.");
$requireSettings = true;
$gq = $db->query("SELECT `g`.*, `oc`.*, `gs`.* " .
	"FROM `gangs` AS `g` " .
	"LEFT JOIN `orgcrimes` AS `oc` ON (`g`.`gangCRIME` = `oc`.`ocID`) " .
	"LEFT JOIN `gang_settings` AS `gs` ON (`gs`.`gangid` = `g`.`gangID`) " .
	"WHERE `g`.`gangID` = ".$ir['gang']);
if(!$db->num_rows($gq)) {
	$db->query("UPDATE `users` SET `gang` = 0 WHERE `gang` = ".$ir['gang']);
	$mtg->error("Your Gang doesn't exist");
}
$gangdata = $db->fetch_row($gq);
require_once(__DIR__ . '/includes/class_mtg_functions_gangs.php');
?><h3><u>Your Gang - <a href='yourgang.php'><?php echo $mtg->format($gangdata['gangNAME']);?></a></h3><?php
if($requireSettings)
	if(empty($gangdata['armoury_is_closed']) && (!isset($_GET['act2']) || $_GET['act2'] != 'editarmoury'))
		$mtg->error($mtg->username($gangdata['gangPRESIDENT'])." <strong>must</strong> set up the Gang armoury before you can access this.".($gangdata['gangPRESIDENT'] == $ir['userid'] ? "<br /><a href='yourgang.php?action=staff&amp;act2=editarmoury'>Do so here</a>" : ''));
$wq = $db->query("SELECT COUNT(`warID`) FROM `gangwars` WHERE `warDECLARER` = ".$ir['gang']." OR `warDECLARED` = ".$ir['gang']);
if($db->fetch_single($wq)) {
	?><hr /><h3><a href='yourgang.php?action=warview' style='color: red;'>Your Gang is currently in <?php echo $db->fetch_single($wq)." war".s($db->fetch_single($wq));?></a></h3><hr /><?php
}
$_GET['action'] = isset($_GET['action']) && ctype_alpha($_GET['action']) ? strtolower(trim($_GET['action'])) : null;
switch($_GET['action']) {
	case "idx":
		gang_index();
		break;
	case "summary":
		gang_summary();
		break;
	case "members":
		gang_memberlist();
		break;
	case "kick":
		gang_staff_kick();
		break;
	case "donate":
		gang_donate();
		break;
	case "donate2":
		gang_donate2();
		break;
	case "warview":
		gang_warview();
		break;
	case "staff":
		gang_staff();
		break;
	case "leave":
		gang_leave();
		break;
	case "atklogs":
		gang_atklogs();
		break;
	case "crimes":
		gang_crimes();
		break;
	case "forums":
		gang_forums();
		break;
	case 'viewarmoury':
		gang_view_armoury();
		break;
	case 'borrowitem':
		gang_borrow_item();
		break;
	case 'donateitem':
		gang_donate_item();
		break;
	default:
		gang_index();
		break;
}
function gang_index() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	$selectEvents = $db->query("SELECT `gevTIME`, `gevTEXT` FROM `gangevents` WHERE `gevGANG` = ".$ir['gang']." ORDER BY `gevTIME` DESC LIMIT 10");
	?><table width='75%' class='table'>
		<tr>
			<th colspan='2'>Actions</th>
		</tr>
		<tr>
			<td><a href='yourgang.php?action=summary'>Summary</a></td>
			<td><a href='yourgang.php?action=donate'>Donate</a></td>
		</tr>
		<tr>
			<td><a href='yourgang.php?action=members'>Members</a></td>
			<td><a href='yourgang.php?action=crimes'>Crimes</a></td>
		</tr>
		<tr>
			<td><a href='yourgang.php?action=viewarmoury'>Armoury</a></td>
			<td><a href='yourgang.php?action=leave'>Leave</a></td>
		</tr>
		<tr>
			<td><a href='yourgang.php?action=atklogs'>Attack Logs</a></td>
			<td><?php echo $gangs->isLeader(true) ? "<a href='yourgang.php?action=staff&amp;act2=idx'>Staff Room</a>" : "&nbsp;";?></td>
		</tr>
	</table><br />
	<table width='75%' class='table'>
		<tr>
			<th>Gang Picture</th>
		</tr>
		<tr>
			<td><?php echo !empty($gangdata['gangBANNER']) ? "<img src='".$mtg->format($gangdata['gangBANNER'])."' alt='Gang Picture' width='150' height='150' />" : 'No Banner';?></td>
		</tr>
	</table><br />
	<table width='75%' class='table'>
		<tr>
			<th>Gang Announcement</th>
		</tr>
		<tr>
			<td><?php echo !empty($gangdata['gangAMENT']) ? $mtg->format($gangdata['gangAMENT']) : "No Announcement";?></td>
		</tr>
	</table><br /><hr width='75%' />
	<strong>Last 10 Gang Events</strong>
	<hr width='75%' />
	<table width='75%' class='table'>
		<tr>
			<th>Time</th>
			<th>Event</th>
		</tr><?php
		if(!$db->num_rows($selectEvents))
			echo "<tr><td colspan='2' class='center'>There are no events available</td></tr>";
		else
			while($r = $db->fetch_row($q)) {
				?><tr>
					<td><?php echo date('F j Y, g:i:s a', $r['gevTIME']);?></td>
					<td><?php echo stripslashes($r['gevTEXT']);?></td>
				</tr><?php
			}
	?></table><?php
}
function gang_summary() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	$selectMembers = $db->query("SELECT COUNT(`userid`) FROM `users` WHERE `gang` = ".$gangdata['gangID']);
	?><strong><u>General</u></strong><hr width='75%' />
	President: <?php echo $mtg->ifExists($gangdata['gangPRESIDENT']) ? $mtg->username($gangdata['gangPRESIDENT']) : 'None';?><br />
	Vice-President: <?php echo $mtg->ifExists($gangdata['gangVICEPRES']) ? $mtg->username($gangdata['gangVICEPRES']) : 'None';?><br />
	Members/Capacity: <?php echo $mtg->format($db->fetch_single($selectMembers));?>/<?php echo $mtg->format($gangdata['gangCAPACITY']);?><br />
	Respect: <?php echo $mtg->format($gangdata['gangRESPECT']);?><br/>
	War Status: <?php echo $gangdata['gangWAR'] == 'Peaceful' ? "<span style='color:green;'>Peaceful</span>" : "<span style='color:red;'>Aggressive</span>";?><br />
	<hr width='75%' />
	<strong>Financial:</strong><br />
	Money in vault: <?php echo money_formatter($gangdata['gangMONEY']);?><br />
	Crystals in vault: <?php echo $mtg->format($gangdata['gangCRYSTALS']);
}
function gang_memberlist() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	?><table width='75%' class='table'>
		<tr>
			<th>User</th>
			<th>Level</th>
			<th>Days In Gang</th><?php
			if($gangs->isLeader(true))
				echo "<th>Stats</th>";
			?><th>&nbsp;</th>
		</tr><?php
	$q = $db->query("SELECT `u`.`userid`, `u`.`daysingang`, `u`.`level`, `u`.`gang`, `us`.`strength`, `us`.`agility`, `us`.`guard`, `us`.`labour`, `us`.`IQ` " .
		"FROM `users` AS `u` " .
		"LEFT JOIN `userstats` AS `us` ON `u`.`userid` = `us`.`userid` " .
		"WHERE `u`.`gang` = ".$gangdata['gangID']." " .
		"ORDER BY `u`.`daysingang` DESC, `u`.`level` DESC");
	if(!$db->num_rows($q))
		echo "<tr><td colspan='".($gangs->isLeader(true) ? 4 : 3)."' class='center'>Your Gang has no members - there's a serious issue here!</td></tr>";
	else
		while($r = $db->fetch_row($q)) {
			?><tr>
				<td><?php echo $mtg->username($r['userid'], true);?></td>
				<td><?php echo $mtg->format($r['level']);?></td>
				<td><?php echo !$r['daysingang'] ? 'Newbie' : $mtg->time_format($r['daysingang'] * 86400);?></td><?php
				if($gangs->isLeader(true)) {
					?><td>
						<strong>Strength:</strong> <?php echo $mtg->format($r['strength']);?><br />
						<strong>Agility:</strong> <?php echo $mtg->format($r['agility']);?><br />
						<strong>Guard:</strong> <?php echo $mtg->format($r['guard']);?><br />
						<strong>Labour:</strong> <?php echo $mtg->format($r['labour']);?><br />
						<strong>IQ:</strong> <?php echo $mtg->format($r['IQ']);?>
					</td><?php
				}
				?><td><?php
					if($gangs->isLeader(true)) {
						?><form action='yourgang.php?action=kick' method='post'>
							<input type='hidden' name='ID' value='<?php echo $r['userid'];?>' />
							<input type='submit' value='Kick' />
						</form><?php
					}
				?></td>
			</tr><?php
		}
	?></table><?php
}
function gang_staff_kick() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	if(!$gangs->isLeader(true))
		$mtg->error("You don't have permission to access this");
	$_POST['ID'] = isset($_POST['ID']) && ctype_digit($_POST['ID']) ? $_POST['ID'] : 0;
	$who         = $_POST['ID'];
	if($who == $gangdata['gangPRESIDENT'])
		$mtg->error("The Gang president cannot be kicked.");
	if($who == $userid)
		$mtg->error("You cannot kick yourself. If you wish to leave, transfer your powers to someone else and then leave like normal.");
	if(!$gangs->isMember($_POST['ID']))
		$mtg->error("Either that player isn't a member of your Gang, or they don't exist");
	$target = $mtg->username($_POST['ID']);
	$me = $mtg->username($_POST['ID']);
	$db->query("UPDATE `users` SET `gang` = 0, `daysingang` = 0 WHERE `userid` = ".$who);
	$gangs->gang_event_add($gangdata['gangID'], $target." was kicked from ".$mtg->format($gangdata['gangNAME']));
	event_add($_POST['ID'], "You've been kicked from <a href='gangs.php?action=view&amp;ID=".$gangdata['gangID']."'>".$mtg->format($gangdata['gangNAME'])."</a> by ".$mtg->username($ir['userid']));
	$mtg->success("You've kicked ".$target." from your Gang");
	gang_memberlist();
}
function gang_donate() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	?><strong>Enter the amounts you wish to donate.</strong>
	<hr width='75%' />
	You have <?php echo money_formatter($ir['money']);?> and <?php echo $mtg->format($ir['crystals']);?> crystal<?php echo $mtg->s($ir['crystals']);?>.
	<hr width='75%'>
	<form action='yourgang.php?action=donate' method='post'>
		<table width='75%' class='table'>
			<tr>
				<th width='25%'>Cash</th>
				<td width='75%'><input type='text' name='money' value='<?php echo $mtg->format($ir['money']);?>' /></td>
			</tr>
			<tr>
				<th>Crystals</th>
				<td><input type='text' name='crystals' value='<?php echo $mtg->format($ir['crystals']);?>' /></td>
			</tr>
			<tr>
				<td colspan='2' class='center'><input type='submit' name='submit' value='Donate' /></td>
			</tr>
		</table>
	</form><?php
	if(array_key_exists('submit', $_POST)) {
		$_POST['money'] = isset($_POST['money']) && ctype_digit(str_replace(',', '', $_POST['money'])) ? str_replace(',', '', $_POST['money']) : 0;
		$_POST['crystals'] = isset($_POST['crystals']) && ctype_digit(str_replace(',', '', $_POST['crystals'])) ? str_replace(',', '', $_POST['crystals']) : 0;
		if(!$_POST['money'] && !$_POST['crystals'])
			$mtg->error("Invalid amount, please go back and try again.");
		if($_POST['money'] > $ir['money'])
			$mtg->error("You can't donate more money than you have, please go back and try again.");
		if($_POST['crystals'] > $ir['crystals'])
			$mtg->error("You can't donate more crystals than you have, please go back and try again.");
		$db->query("UPDATE `users` SET `money` = `money` - ".$_POST['money'].", `crystals` = `crystals` - ".$_POST['crystals']." WHERE `userid` = ".$ir['userid']);
		$db->query("UPDATE `gangs` SET `gangMONEY` = `gangMONEY` + ".$_POST['money'].", `gangCRYSTALS` = `gangCRYSTALS` + ".$_POST['crystals']." WHERE `gangID` = ".$gangdata['gangID']);
		$what = '';
		if($_POST['money'])
			$what .= ', '.money_formatter($_POST['money']);
		if($_POST['crystals'])
			$what .= ', '.$mtg->format($_POST['crystals']).' crystal'.$mtg->s($_POST['crystals']);
		$what = substr($what, 0, -2);
		$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." donated ".$what." to the Gang");
		$mtg->success("You've donated ".$what." to ".$mtg->format($gangdata['gangNAME']));
	}
}
function gang_leave() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	if($gangs->isLeader(true))
		$mtg->error("You cannot leave while you are still ".($ir['userid'] == $gangdata['gangVICEPRES'] ? 'vice-' : '')."president of your Gang.");
	if(isset($_POST['submit']) && $_POST['submit'] == 'Yes, leave!') {
		$db->query("UPDATE `users` SET `gang` = 0, `daysingang` = 0 WHERE `userid` = ".$ir['userid']);
		$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." has left the Gang");
		$mtg->success("You've left ".$mtg->format($gangdata['gangNAME']));
	} else {
		?>Are you sure you wish to leave your Gang?
		<hr width='75%' />
		<form action='yourgang.php?action=leave' method='post'>
			<input type='submit' name='submit' value='Yes, leave!' />
			<hr width='75%' />
			<input type='submit' name='submit' value='No, stay!' onclick=\"window.location='yourgang.php';\" />
		</form><?php
	}
}
function gang_warview() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	$wq = $db->query("SELECT * FROM `gangwars` WHERE `warDECLARER` = ".$gangdata['gangID']." OR `warDECLARED` = ".$gangdata['gangID']);
	?><strong>These are the wars your Gang is in.</strong>
	<hr width='75%' />
	<table width='75%' class='table'>
		<tr>
			<th>Time Started</th>
			<th>Versus</th>
			<th>Who Declared</th>
			<th>Members</th>
		</tr><?php
		if(!$db->num_rows($wq))
			echo "<tr><td colspan='4' class='center'>Your Gang isn't in any wars</td></tr>";
		else
			while($r = $db->fetch_row($wq)) {
				if($gangdata['gangID'] == $r['warDECLARER']) {
					$w = 'You';
					$f = 'warDECLARED';
				} else {
					$w = 'Them';
					$f = 'warDECLARER';
				}
				$q = $db->query("SELECT `userid` FROM `users` WHERE `gang` =  ".$r[$f]." ORDER BY `daysingang` DESC, `level` DESC");
				$ggq  = $db->query("SELECT `gangID`, `gangNAME` FROM `gangs` WHERE `gangID` = ".$r[$f]);
				$them = $db->num_rows($ggq) ? $db->fetch_row($ggq) : array('gangID' => 0, 'gangNAME' => 'Deleted Gang');
				?><tr>
					<td><?php echo date('F j, Y, g:i:s a', $r['warTIME']);?></td>
					<td><?php echo $gangs->display($them['gangID'], $them['gangNAME']);?></td>
					<td><?php echo $w;?></td>
					<td>
						<table class='100%' class='center'>
							<tr>
								<th width='33%'>Member</th>
								<th width='34%'>Location</th>
								<th width='33%'>Action</th>
							</tr><?php
								if(!$db->num_rows($q))
									echo $gangs->display($them['gangID'], $them['gangNAME'])." doesn't have any members";
								else
									while($r = $db->fetch_row($q)) {
										?><tr>
											<td><?php echo $mtg->username($r['userid']);?></td>
											<td><?php echo $mtg->showLocation($r['userid']);?></td>
											<td><a href='attack.php?ID=<?php echo $r['userid'];?>'>Attack</a></td>
										</tr><?php
									}
						?></table>
					</td>
				</tr><?php
			}
	?></table><?php
}
function gang_atklogs() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	$atks = $db->query("SELECT `a`.*, `u1`.`gang` AS `attacker_gang`, `u2`.`gang` AS `attacked_gang` " .
		"FROM `attacklogs` AS `a` " .
		"INNER JOIN `users` AS `u1` ON `a`.`attacker` = `u1`.`userid` " .
		"INNER JOIN `users` AS `u2` ON `a`.`attacked` = `u2`.`userid` " .
		"WHERE (`u1`.`gang` = ".$gangdata['gangID']." OR `u2`.`gang` = ".$gangdata['gangID'].") AND `result` = 'won' " .
		"ORDER BY `time` DESC LIMIT 50");
	?><strong>Attack Logs - The last 50 attacks involving someone in your Gang</strong>
	<hr width='75%' />
	<table width='75%' class='table'>
		<tr>
			<th width='25%'>Time</th>
			<th width='75%'>Attack</th>
		</tr><?php
		if(!$db->num_rows($atks))
			echo "<tr><td colspan='2' class='center'>There are no logs available</td></tr>";
		else
			while($r = $db->fetch_row($atks)) {
				?><tr>
					<td><?php echo date('F j, Y, g:i:s a', $r['time']);?></td>
					<td><?php echo $mtg->username($r['attacker']);?> <span style='color:<?php echo $r['attacker_gang'] == $ir['gang'] ? 'green' : 'red';?>;'>attacked</span> <?php echo $mtg->username($r['attacked']);?></td>
				</tr><?php
			}
	?></table><?php
}
function gang_crimes() {
	global $gangdata, $mtg;
	echo !$gangdata['gangCRIME']
		? "Your Gang isn't currently planning a crime"
		: "Your Gang is currently planning the organised crime: &ldquo;".$mtg->format($gangdata['ocNAME'])."&rdquo;<br /> There ".($gangdata['gangCHOURS'] == 1 ? 'is' : 'are')." ".$mtg->time_format($gangdata['gangCHOURS'] * 3600);
}
function gang_staff() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	if(!$gangs->isLeader(true))
		$mtg->error("You don't have permission to be here");
	$_GET['act2'] = isset($_GET['act2']) && ctype_alpha($_GET['act2']) ? strtolower(trim($_GET['act2'])) : 'idx';
	switch($_GET['act2']) {
		case "idx":
			gang_staff_idx();
			break;
		case "apps":
			gang_staff_apps();
			break;
		case "vault":
			gang_staff_vault();
			break;
		case "vicepres":
			gang_staff_vicepres();
			break;
		case "pres":
			gang_staff_pres();
			break;
		case "upgrade":
			gang_staff_upgrades();
			break;
		case "declare":
			gang_staff_wardeclare();
			break;
		case "status":
			gang_staff_status();
			break;
		case "surrender":
			gang_staff_surrender();
			break;
		case "viewsurrenders":
			gang_staff_viewsurrenders();
			break;
		case "crimes":
			gang_staff_orgcrimes();
			break;
		case "massmailer":
			gang_staff_massmailer();
			break;
		case "desc":
			gang_staff_desc();
			break;
		case "ament":
			gang_staff_ament();
			break;
		case "name":
			gang_staff_name();
			break;
		case "tag":
			gang_staff_tag();
			break;
		case "banner":
			gang_staff_banner();
			break;
		case "banner":
			gang_dobanner_change();
			break;
		case "masspayment":
			gang_staff_masspayment();
			break;
		case 'editarmoury':
			gang_staff_edit_armoury_settings();
			break;
		case 'trash':
			gang_staff_armoury_trash_item();
			break;
		case 'trashall':
			gang_staff_armoury_trash_all_items();
			break;
		case 'takeall':
			gang_staff_armoury_take_all_items();
			break;
		case 'recall':
			gang_staff_armoury_recall_item();
			break;
		case 'leaditem':
			gang_staff_armoury_leader_take_item();
			break;
		default:
			gang_staff_idx();
			break;
	}
}
function gang_staff_idx() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	?><table width='75%' class='table'>
		<tr>
			<th colspan='4'>General</th>
		</tr>
		<tr>
			<td><a href='yourgang.php?action=staff&amp;act2=vault'>Vault Management</a></td>
			<td><a href='yourgang.php?action=staff&amp;act2=apps'>Application Management</a></td>
			<td><a href='yourgang.php?action=staff&amp;act2=vicepres'>Change Vice-President</a></td>
			<td><a href='yourgang.php?action=staff&amp;act2=upgrade'>Upgrade Gang</a></td>
		</tr>
		<tr>
			<td><a href='yourgang.php?action=staff&amp;act2=crimes'>Organised Crimes</a></td>
			<td><a href='yourgang.php?action=staff&amp;act2=masspayment'>Mass Payment</a></td>
			<td><a href='yourgang.php?action=staff&amp;act2=ament'>Change Gang Announcement</a></td>
			<td>&nbsp;</td>
		</tr>
	</table><?php
	if($gangs->isLeader()) {
		?><hr width='75%' />
		<table width='75%' class='table'>
			<tr>
				<td><a href='yourgang.php?action=staff&amp;act2=pres'>Change President</a></td>
				<td><a href='yourgang.php?action=staff&amp;act2=declare'>Declare War</a></td>
				<td><a href='yourgang.php?action=staff&amp;act2=surrender'>Surrender</a></td>
				<td><a href='yourgang.php?action=staff&amp;act2=viewsurrenders'>View or Accept Surrenders</a></td>
			</tr>
			<tr>
				<td><a href='yourgang.php?action=staff&amp;act2=massmailer'>Mass Mail Gang</a></td>
				<td><a href='yourgang.php?action=staff&amp;act2=name'>Change Gang Name</a></td>
				<td><a href='yourgang.php?action=staff&amp;act2=desc'>Change Gang Desc</a></td>
				<td><a href='yourgang.php?action=staff&amp;act2=tag'>Change Gang Tag</a></td>
			</tr>
			<tr>
				<td><a href='yourgang.php?action=staff&amp;act2=banner'>Change Gang Banner</a></td>
				<td><a href='yourgang.php?action=staff&amp;act2=editarmoury'>Change Armoury Settings</a></td>
				<td><a href='yourgang.php?action=staff&amp;act2=status'>Change Gang Status</a></td>
					<td>&nbsp;</td>
			</tr>
		</table><?php
	}
}
function gang_staff_apps() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	if(!$gangs->isLeader(true))
		$mtg->error("You don't have permission to be here");
	$_POST['app']   = isset($_POST['app']) && ctype_digit($_POST['app']) ? $_POST['app'] : null;
	$_POST['what'] = isset($_POST['what']) && in_array($_POST['what'], ['accept', 'decline']) ? $_POST['what'] : null;
	if(!empty($_POST['app']) && !empty($_POST['what'])) {
		$aq = $db->query("SELECT `appUSER` FROM `applications` WHERE `appID` = ".$_POST['app']." AND `appGANG` = ".$gangdata['gangID']);
		if(!$db->num_rows($aq))
			$mtg->error("Either that application isn't yours, or it doesn't exist");
		$appdata = $db->fetch_row($aq);
		$target = $mtg->username($appdata['appUSER']);
		if($_POST['what'] == 'decline')
			$what = 'declined';
		else {
			$cnt = $db->query("SELECT COUNT(`userid`) FROM `users` WHERE `gang` = ".$gangdata['gangID']);
			if($gangdata['gangCAPACITY'] <= $db->fetch_single($cnt))
				$mtg->error("Your Gang is full, you must upgrade it to hold more before you can accept another user!");
			$selectGang = $db->query("SELECT `gang` FROM `users` WHERE `userid` = ".$appdata['appUSER']);
			if($db->fetch_single($selectGang))
				$mtg->error("That person is already in a Gang.");
			$db->query("UPDATE `users` SET `gang` = ".$gangdata['gangID'].", `daysingang` = 0 WHERE `userid` = ".$appdata['appUSER']);
			$what = 'accepted';
		}
		$db->query("DELETE FROM `applications` WHERE `appID` = ".$_POST['app']);
		event_add($appdata['appUSER'], "Your application to join &ldquo;".$gangs->display($gangdata['gangID'], $gangdata['gangNAME'])."&rdquo; was ".$what);
		$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." has ".$what." ".$target."'s application to join the Gang");
		$mtg->success("You've ".$what." ".$target."'s application to join ".$gangs->display($gangdata['gangID'], $gangdata['gangNAME']));
		?><hr width='75%' /><?php
	}
	$q = $db->query("SELECT `appID`, `appTEXT`, `userid`, `username`, `level`, `money` " .
		"FROM `applications` AS `a` " .
		"INNER JOIN `users` AS `u` ON `a`.`appUSER` = `u`.`userid` " .
		"WHERE `a`.`appGANG` = ".$gangdata['gangID']);
	?><table width='75%' class='table'>
		<tr>
			<th colspan='3'>Applications</th>
		</tr>
		<tr>
			<th width='25%'>Applicant</th>
			<th width='65%'>Application</th>
			<th width='10%'>Decision</th>
		</tr><?php
		if(!$db->num_rows($q))
			echo "<tr><td colspan='3' class='center'>There are no applications</td></tr>";
		else
			while($r = $db->fetch_row($q)) {
				?><tr>
					<td>
						<strong>Name:</strong> <?php echo $mtg->username($r['appUSER'], true);?><br />
						<strong>Level:</strong> <?php echo $mtg->format($r['level'], true);?><br />
						<strong>Cash:</strong> <?php echo money_formatter($r['money']);?>
					</td>
					<td><?php echo $mtg->format($r['appTEXT'], true);?></td>
					<td>
						<form action='yourgang.php?action=staff&amp;act2=apps' method='post'>
							<input type='hidden' name='app' value='<?php echo $r['appID'];?>' />
							<table class='table' width='100%'>
								<tr>
									<td><input type='radio' name='what' value='accept' checked='checked' />Accept</td>
								</tr>
								<tr>
									<td><input type='radio' name='what' value='decline' />Decline</td>
								</tr>
								<tr>
									<td><input type='submit' name='submit' value='Decide' /></td>
								</tr>
							</table>
						</form>
					</td>
				</tr><?php
			}
	?></table><?php
}
function gang_staff_vault() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	$_POST['user'] = isset($_POST['user']) && ctype_digit($_POST['user']) ? $_POST['user'] : null;
	if(!empty($_POST['user'])) {
		$_POST['crystals'] = isset($_POST['crystals']) && ctype_digit(str_replace(',', '', $_POST['crystals'])) ? str_replace(',', '', $_POST['crystals']) : 0;
		$_POST['money'] = isset($_POST['money']) && ctype_digit(str_replace(',', '', $_POST['money'])) ? str_replace(',', '', $_POST['money']) : 0;
		if(!$_POST['money'] && !$_POST['crystals'])
			$mtg->error("You didn't enter anything valid");
		if($_POST['crystals'] > $gangdata['gangCRYSTALS'])
			$mtg->error("The vault does not have that many crystals!");
		if($_POST['money'] > $gangdata['gangMONEY'])
			$mtg->error("The vault does not have that much money!");
		if(!$gangs->isMember($_POST['user']))
			$mtg->error("Either that player's not a member of your gang, or they don't exist");
		$target = $mtg->username($_POST['user']);
		$db->query("UPDATE `users` SET `money` = `money` + ".$_POST['money'].", `crystals` = `crystals` + ".$_POST['crystals']. " WHERE `userid` = ".$_POST['user']);
		$db->query("UPDATE `gangs` SET `gangMONEY` = `gangMONEY` - ".$_POST['money'].", `gangCRYSTALS` = `gangCRYSTALS` - ".$_POST['crystals']." WHERE `gangID` = ".$gangdata['gangID']);
		$what = '';
		if($_POST['money'])
			$what .= ', '.money_formatter($_POST['money']);
		if($_POST['crystals'])
			$what .= ', '.$mtg->format($_POST['crystals']).' crystal'.$mtg->s($_POST['crystals']);
		$what = substr($what, 0, -2);
		$gangs->gang_event_add($gangdata['gangID'], $target." was paid ".$what." from the Gang vault");
		event_add("You've been paid ".$what." from your Gang's vault");
		$mtg->success("You've paid ".$what." to ".$target);
	}
	?>The vault has <?php echo money_formatter($gangdata['gangMONEY']);?> and <?php echo $mtg->format($gangdata['gangCRYSTALS']);?> crystal<?php echo $mtg->s($gangdata['gangCRYSTALS']);?>.<hr />
	<form action='yourgang.php?action=staff&amp;act2=vault' method='post'>
		<table class='table' width='75%'>
			<tr>
				<th width='25%'>Member</th>
				<td width='75%'><?php echo $gangs->listMembers();?></td>
			</tr>
			<tr>
				<th>Money</th>
				<td><input type='text' name='money' value='<?php echo $mtg->format($gangdata['gangMONEY']);?>' /></td>
			</tr>
			<tr>
				<th>Crystals</th>
				<td><input type='text' name='crystals' value='<?php echo $mtg->format($gangdata['gangCRYSTALS']);?>' /></td>
			</tr>
			<tr>
				<td colspan='2' class='center'><input type='submit' name='submit' value='Give' /></td>
			</tr>
		</table>
	</form><?php
}
function gang_staff_vicepres() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	if(isset($_POST['subm'])) {
		$_POST['vp1'] = isset($_POST['vp1']) && ctype_digit($_POST['vp1']) ? $_POST['vp1'] : null;
		$_POST['vp2'] = isset($_POST['vp2']) && ctype_digit($_POST['vp2']) ? $_POST['vp2'] : null;
		if(empty($_POST['vp1']) && empty($_POST['vp2']))
			$mtg->error("You didn't make a valid selection");
		if(!empty($_POST['vp1']) && !empty($_POST['vp2']))
			$mtg->error("Use only one option");
		$newVP = empty($_POST['vp1']) ? $_POST['vp2'] : $_POST['vp1'];
		if(!$gangs->isMember($newVP))
			$mtg->error("Either that player isn't a member of your gang, or they don't exist");
		$target = $mtg->username($newVP);
		$db->query("UPDATE `gangs` SET `gangVICEPRES` = ".$newVP." WHERE `gangID` = ".$gangdata['gangID']);
		event_add($newVP, "You were transferred vice-presidency of ".$gangs->display($gangdata['gangID'], $gangdata['gangNAME']));
		$gangs->gang_event_add($gangdata['gangID'], $target." has been transferred vice-presidency of ".$gangs->display($gangdata['gangID'], $gangdata['gangNAME']));
		$mtg->success("You've transferred vice-presidency of ".$gangs->display($gangdata['gangID'], $gangdata['gangNAME'])." to ".$target);
	} else {
		?><form action='yourgang.php?action=staff&amp;act2=vicepres' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Vice President</th>
					<td width='75%'><?php echo $gangs->listMembers($gangdata['gangID'], 'vp1', true);?></td>
				</tr>
				<tr>
					<th><span style='font-weight:700;text-decoration:underlined;'>OR</span> enter a member's ID</th>
					<td><input type='number' name='vp2' value='0' /></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='subm' value='Transfer Vice-Presidency' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_staff_status() {
		global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	$opts = ['aggressive', 'peaceful'];
	if(isset($_POST['submit'])) {
		if($gangdata['gangWAR'] == 'Aggressive')
			$mtg->error("You can't change from an aggressive stance");
		$_POST['status'] = isset($_POST['status']) && ctype_alpha($_POST['status']) && in_array($_POST['status'], $opts) ? strtolower(trim($_POST['status'])) : null;
		if(empty($_POST['status']))
			$mtg->error("You didn't make a valid selection");
		$db->query("UPDATE `gangs` SET `gangWAR` = '".ucfirst($_POST['status'])."' WHERE `gangID` = ".$gangdata['gangID']);
		$mtg->success("You've set your gang's warring status as ".$_POST['status']);
	} else {
		?><strong>Note:</strong> Changing your gang's warring stance to aggressive is permanent. It cannot be changed back<br />
		<form action='yourgang.php?action=staff&amp;act2=status' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Status</th>
					<td width='75%'><?php
						foreach($opts as $opt)
							printf("<input type='radio' name='status' value='%s'%s />%s<br />", $opt, $opt == $gangdata['gangWAR'] ? " checked='checked'" : '', ucfirst($opt));
					?></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='submit' value='Submit' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_staff_wardeclare() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	if(isset($_POST['subm'])) {
		$_POST['gang'] = isset($_POST['gang']) && ctype_digit($_POST['gang']) ? $_POST['gang'] : 0;
		if($_POST['gang'] == $gangdata['gangID'])
			$mtg->error("You can't declare war on your own Gang.");
		$query = $db->query("SELECT `gangID`, `gangNAME`, `gangWAR` FROM `gangs` WHERE `gangID` = ".$_POST['gang']);
		if(!$db->num_rows($query))
			$mtg->error("That gang doesn't exist");
		$war = $db->fetch_row($query);
		if($war['gangWAR'] == 'Peaceful')
			$mtg->error($gangs->display($war['gangID'], $war['gangNAME'])." is peaceful. You can't declare war on them");
		$db->query("INSERT INTO `gangwars` VALUES(NULL, ".$gangdata['gangID'].", ".$_POST['gang'].", ".time().")");
		$event = $gangs->display($gangdata['gangID'], $gangdata['gangNAME'])." has declared war on ".$gangs->display($war['gangID'], $war['gangNAME']);
		$gangs->gang_event_add($gangdata['gangID'], $event);
		$gangs->gang_event_add($war['gangID'], $event);
		$mtg->success("You've declared war on ".$gangs->display($war['gangID'], $war['gangNAME']));
	}
	$selectGangs = $db->query("SELECT `gangID`, `gangNAME` FROM `gangs` WHERE `gangID` <> ".$gangdata['gangID']);
	if(!$db->num_rows($selectGangs))
		$mtg->error("There are no other gangs to war");
	$gangs = [];
	while($row = $db->fetch_row($selectGangs))
		$gangs[] = $row['gangID'];
	$selectWars1 = $db->query("SELECT `warDECLARER` FROM `gangwars` WHERE `warDECLARED` = ".$gangdata['gangID']);
	if($db->num_rows($selectWars1)) {
		$wars1 = [];
		while($row = $db->fetch_row($selectWars1))
			$wars1[] = $row['warDECLARER'];
		$new1 = array_values(array_unique($gangs, $wars1));
	}
	$selectWars2 = $db->query("SELECT `warDECLARED` FROM `gangwars` WHERE `warDECLARER` = ".$gangdata['gangID']);
	if($db->num_rows($selectWars2)) {
		$wars2 = [];
		while($row = $db->fetch_row($selectWars2))
			$wars2[] = $row['warDECLARED'];
		$new2 = array_values(array_unique($gangs, $wars2));
	}
	if(isset($new1) && isset($new2))
		$gangIDs = array_values(array_unique($new1, $new2));
	if(isset($gangIDs)) {
		$selectGangs = $db->query("SELECT `gangID`, `gangNAME` FROM `gangs` WHERE `gangID` NOT IN(".implode(', ', $gangIDs).") AND `gangID` <> ".$gangdata['gangID']);
		if(!$db->num_rows($selectGangs))
			$mtg->error("There are no other gangs to war - you're either already at war with every other gang, or there are no others to war against!");
	}
	?><form action='yourgang.php?action=staff&amp;act2=declare' method='post'>
		<table class='table' width='75%'>
			<tr>
				<th width='25%'>Gang</th>
				<td width='75%'><select name='gang'><?php
					while($row = $db->fetch_row($selectGangs))
						printf("<option value='%u'>%s</option>", $row['gangID'], $mtg->format($row['gangNAME']));
				?></select></td>
			</tr>
			<tr>
				<td colspan='2' class='center'><input type='submit' name='subm' value='Declare' /></td>
			</tr>
		</table>
	</form><?php
}
function gang_staff_surrender() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	if(!isset($_POST['subm'])) {
		$wq = $db->query("SELECT * FROM `gangwars` WHERE `warDECLARER` = ".$gangdata['gangID']." OR `warDECLARED` = ".$gangdata['gangID']);
		if(!$db->num_rows($wq))
			$mtg->error("You're not at war");
		?><form action='yourgang.php?action=staff&amp;act2=surrender' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Surrender</th>
					<td width='75%'><select name='war'><?php
						while($r = $db->fetch_row($wq)) {
							$f = $gangdata['gangID'] == $r['warDECLARER'] ? 'warDECLARED' : 'warDECLARER';
							$selectGangName = $db->query("SELECT `gangNAME` FROM `gangs` WHERE `gangID` = ".$r[$f]);
							printf("<option value='%u'>%s</option>", $r[$f], $db->num_rows($selectGangName) ? $mtg->format($db->fetch_single($selectGangName)) : 'Deleted gang');
						}
					?></select></td>
				</tr>
				<tr>
					<th>Message</th>
					<td><input type='text' name='msg' size='80%' /></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='subm' value='Surrender' /></td>
				</tr>
			</table>
		</form><?php
	} else {
		$_POST['war'] = isset($_POST['war']) && ctype_digit($_POST['war']) ? $_POST['war'] : null;
		if(empty($_POST['war']))
			$mtg->error("You didn't select a valid war");
		$wq           = $db->query("SELECT * FROM `gangwars` WHERE `warID` = ".$_POST['war']);
		if(!$db->num_rows($wq))
			$mtg->error("That war doesn't exist");
		$r = $db->fetch_row($wq);
		if(!in_array($gangdata['gangID'], [$r['warDECLARER'], $r['warDECLARED']]))
			$mtg->error("That is not your war");
		if($gangdata['gangID'] == $r['warDECLARER']) {
			$w = "You";
			$f = "warDECLARED";
		} else {
			$w = "Them";
			$f = "warDECLARER";
		}
		$db->query("INSERT INTO `surrenders` VALUES (NULL, ".$_POST['war'].", ".$gangdata['gangID'].", ".$r[$f].", '".$db->escape($_POST['msg'])."')");
		$event = $gangs->display($gangdata['gangID'], $gangdata['gangNAME'])." have sent a Notice of Surrender to ".$gangs->display($r[$f]);
		$gangs->gang_event_add($gangdata['gangID'], $event);
		$gangs->gang_event_add($r[$f], $event);
		$mtg->success("You have asked to surrender.");
	}
}
function gang_staff_viewsurrenders() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	if(isset($_POST['subm'])) {
		$_POST['sur'] = isset($_POST['sur']) && ctype_digit($_POST['sur']) ? $_POST['sur'] : null;
		if(empty($_POST['sur']))
			$mtg->error("You didn't select a valid surrender");
		$q  = $db->query("SELECT `w`.`warID` " .
			"FROM `surrenders` AS `s` " .
			"INNER JOIN `gangwars` AS `w` ON `s`.`surWAR` = `w`.`warID` " .
			"WHERE `surID` = ".$_POST['sur']." AND `surTO` = ".$gangdata['gangID']);
		if(!$db->num_rows($q))
			$mtg->error("Either that surrender doesn't exist, or it's not yours");
		$surr = $db->fetch_row($q);
		$db->free_result($q);
		$warID = $surr['warID'];
		$f = $gangdata['gangID'] == $r['warDECLARER'] ? 'warDECLARED' : 'warDECLARER';
		$db->query("DELETE FROM `surrenders` WHERE `surWAR` = ".$surr['warID']);
		$db->query("DELETE FROM `gangwars` WHERE `warID` = ".$surr['warID']);
		$event = $gangs->display($gangdata['gangID'], $gangdata['gangNAME'])." have accepted the surrender from ".$gangs->display($r[$f]).". The war is over";
		$gangs->gang_event_add($gangdata['gangID'], $event);
		$gangs->gang_event_add($r[$f], $event);
		$mtg->success("You have accepted the surrender from ".$gangs->display($r[$f]).", the war is over.");
	}
	$wq = $db->query("SELECT `surID`, `surMSG`, `w`.* " .
		"FROM `surrenders` AS `s` " .
		"INNER JOIN `gangwars` AS `w` ON `s`.`surWAR` = `w`.`warID` " .
		"WHERE `surTO` = ".$gangdata['gangID']);
	if(!$db->num_rows($wq))
		$mtg->error("There are no pending Notices of Surrender");
	?><form action='yourgang.php?action=staff&amp;act2=viewsurrenders' method='post'>
		<table class='table' width='75%'>
			<tr>
				<th width='25%'>Notice of Surrender</th>
				<td width='75%'><select name='sur'><?php
					while($r = $db->fetch_row($wq)) {
						$f = $gangdata['gangID'] == $r['warDECLARER'] ? 'warDECLARED' : 'warDECLARER';
						$selectGang = $db->query("SELECT `gangNAME` FROM `gangs` WHERE `gangID` = ".$r[$f]);
						printf("<option value='%u'>War vs. %s - Message: %s</option>", $r['surID'], $db->num_rows($selectGang) ? $mtg->format($db->fetch_single($selectGang)) : 'Deleted gang', $mtg->format($r['surMSG']));
					}
				?></select></td>
			</tr>
			<tr>
				<td colspan='2' class='center'><input type='submit' name='subm' value='Accept Surrender' /></td>
			</tr>
		</table>
	</form><?php
}
function gang_staff_orgcrimes() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs, $set;
	$_GET['act3'] = isset($_GET['act3']) && ctype_alpha($_GET['act3']) ? strtolower(trim($_GET['act3'])) : null;
	switch($_GET['act3']) {
		case 'cancel':
			if(!$gangdata['gangCRIME'])
				$mtg->error("Your gang isn't currently planning a crime");
			$timeElapsed = 24 - $gangdata['gangCHOURS'];
			$timeLeft = 24 - $timeElapsed;
			$penalty = ($timeLeft * $gangs->memberCount()) * 1000000;
			if(!array_key_exists('ans', $_GET)) {
				?>You're currently planning to commit &ldquo;<?php echo $mtg->format($gangdata['ocNAME']);?>&rdquo;.<br />
				Your gang will lose <?php echo money_formatter($penalty);?> is planning costs.<br />
				Are you sure you wish to cancel the plans?<br />
				<a href='yourgang.php?action=staff&amp;act2=crimes&amp;act3=cancel&amp;ans=yes'>Yes, cancel the plans</a> &middot; <a href='yourgang.php?action=staff&amp;act2=crimes'>No, take me back</a><?php
			} else {
				$extra = '';
				if(isset($set['gang_oc_cancel_penalty']) && $set['gang_oc_cancel_penalty']) {
					if($penalty > $gangdata['gangMONEY'])
						$mtg->error("Your gang vault doesn't have enough cash. You need a further ".money_formatter($penalty - $gangdata['gangMONEY']));
					$extra = ", `gangMONEY` = `gangMONEY` - ".$penalty;
				}
				$db->query("UPDATE `gangs` SET `gangCHOURS` = 0, `gangCRIME` = 0".$extra." WHERE `gangID` = ".$gangdata['gangID']);
				$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." cancelled the crime plans");
				$mtg->success("You've cancelled the crime plans");
			}
			break;
		case 'start':
			if($gangdata['gangCRIME'])
				$mtg->error("You're already planning to commit &ldquo;".$mtg->format($gangdata['ocNAME'])."&rdquo;");
			$_POST['crime'] = isset($_POST['crime']) && ctype_digit($_POST['crime']) ? $_POST['crime'] : null;
			if(!empty($_POST['crime'])) {
				$select = $db->query("SELECT `ocNAME`, `ocUSERS` FROM `orgcrimes` WHERE `ocID` = ".$_POST['crime']);
				if(!$db->num_rows($select))
					$mtg->error("That crime doesn't exist");
				$row = $db->fetch_row($select);
				$db->query("UPDATE `gangs` SET `gangCRIME` = ".$_POST['crime'].", `gangCHOURS` = 24 WHERE `gangID` = ".$gangdata['gangID']);
				$mtg->success("You have started to plan this crime. It will take 24 hours.");
			} else {
				$q = $db->query("SELECT `ocID`, `ocNAME`, `ocUSERS` FROM `orgcrimes` WHERE `ocUSERS` <= ".$gangs->memberCount());
				if(!$db->num_rows($q))
					$mtg->error("There are no crimes currently available to your gang");
				?><form action='yourgang.php?action=staff&amp;act2=crimes' method='post'>
					<table class='table' width='75%'>
						<tr>
							<th width='25%'>Crime</th>
							<td width='75%'><select name='crime'><?php
								while($row = $db->fetch_row($q))
									printf("<option value='%u'>%s - (%s members required)</option>", $row['ocID'], $mtg->format($row['ocNAME']), $mtg->format($row['ocUSERS']));
							?></select></td>
						</tr>
						<tr>
							<td colspan='2' class='center'><input type='submit' name='submit' value='Start Plans' /></td>
						</tr>
					</table>
				</form><?php
			}
			break;
		default:
			?><h4>Organised Crimes</h4><?php
			echo $gangdata['gangCHOURS']
				? "<a href='yourgang.php?action=staff&amp;act2=crimes&amp;act3=cancel'>Cancel crime planning in progress</a>"
				: "<a href='yourgang.php?action=staff&amp;act2=crimes&amp;act3=start'>Start planning</a>";
			break;
	}
}
function gang_staff_pres() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	if(!$gangs->isLeader())
		$mtg->error("You don't have permission to be here");
	if(isset($_POST['subm'])) {
		$_POST['pres'] = isset($_POST['pres']) && ctype_digit($_POST['pres']) ? $_POST['pres'] : null;
		if(empty($_POST['pres']))
			$mtg->error("You didn't select a valid player");
		if(!$mtg->ifExists($_POST['pres']))
			$mtg->error("That player doesn't exist");
		$target = $mtg->username($_POST['pres']);
		if(!$gangs->isMember($_POST['pres']))
			$mtg->error($target." isn't a member of your gang");
		$db->query("UPDATE `gangs` SET `gangPRESIDENT` = ".$_POST['pres']." WHERE `gangID` = ".$gangdata['gangID']);
		$gangs->gang_event_add($gangdata['gangID'], $target." is now the President of ".$gangs->display($gangdata['gangID'], $gangdata['gangNAME']));
		event_add($_POST['pres'], "You are now the President of ".$gangs->display($gangdata['gangID'], $gangdata['gangNAME']));
		$mtg->success("You've transferred Presidency to ".$target);
	} else {
		?><form action='yourgang.php?action=staff&amp;act2=pres' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>President</th>
					<td width='75%'><?php echo $gangs->listMembers($gangdata['gangID'], 'pres', false, $gangdata['gangPRESIDENT']);?></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='subm' value='Transfer Presidency' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_staff_upgrades() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	$placeCost = 100000; // Alter to whatever price per placement
	$cost = $_POST['membs'] * $placeCost;
	if(isset($_POST['submit'])) {
		$_POST['membs'] = isset($_POST['membs']) && ctype_digit(str_replace(',', '', $_POST['membs'])) ? str_replace(',', '', $_POST['membs']) : null;
		if(empty($_POST['membs']))
			$mtg->error("You didn't enter a valid amount of places");
		if($cost > $gangdata['gangMONEY'])
			$mtg->error("Your gang doesn't have enough cash in the vault to upgrade by ".$mtg->format($_POST['membs']));
		$db->query("UPDATE `gangs` SET `gangCAPACITY` = `gangCAPACITY` + ".$_POST['membs'].", `gangMONEY` = `gangMONEY` - ".$cost." WHERE `gangID` = ".$gangdata['gangID']);
		$gangs->gang_event_add($gangdata['gangID'], "The maximum capacity has been upgraded by ".$mtg->format($_POST['membs']).", setting the new capacity at ".$gangs->memberCount()."/".$mtg->format($gangdata['gangCAPACITY'] + $_POST['membs']));
		$mtg->success("You paid ".money_formatter($cost)." to add ".$mtg->format($_POST['membs'])." places to your gang.");
	} else {
		$remaining = $gangdata['gangCAPACITY'] - $gangs->memberCount();
		?><strong>Current Capacity:</strong> <?php echo $mtg->format($gangdata['gangCAPACITY']);?> (<?php echo $mtg->format($remaining);?> place<?php echo $mtg->s($remaining);?> remaining.<br />
		Each new placement will cost the gang vault <?php echo money_formatter($placeCost);?><br />
		<form action='yourgang.php?action=staff&amp;act2=upgrade' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Capacity</th>
					<td width='75%'><input type='text' name='membs' value='<?php echo $mtg->format(floor($gangdata['gangMONEY'] / $placeCost));?>' /></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='submit' value='Upgrade' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_staff_massmailer() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs, $set;
	$_POST['text'] = isset($_POST['text']) && is_string($_POST['text']) ? $db->escape($_POST['text']) : null;
	if(!empty($_POST['text'])) {
		if(isset($set['gang_min_char_req_mail']) && $set['gang_min_char_req_mail'])
			if($set['gang_min_char_req_mail'] > strlen($_POST['text']))
				$mtg->error("Your message needs to contain at least ".$mtg->format($set['gang_min_char_req_mail'])." character".$mtg->s($set['gang_min_char_req_mail']));
		if(isset($set['gang_max_char_req_mail']) && $set['gang_max_char_req_mail'])
			if(strlen($_POST['text']) > $set['gang_max_char_req_mail'])
				$mtg->error("Your message is longer than the maximum of ".$mtg->format($set['gang_max_char_req_mail'])." character".$mtg->s($set['gang_max_char_req_mail']));
		$_POST['subj'] = isset($_POST['subj']) && is_string($_POST['subj']) ? $db->escape($_POST['subj']) : null;
		$subj = 'Gang Mass Message'.(!empty($_POST['subj']) ? ': '.$_POST['subj'] : '');
		$select = $db->query("SELECT `userid` FROM `users` WHERE `gang` = ".$gangdata['gangID']);
		$cnt = 0;
		while($row = $db->fetch_row($select)) {
			$db->query("INSERT INTO `mail` (`mail_from`, `mail_to`, `mail_subject`, `mail_text`, `mail_time`) VALUES (".$ir['userid'].", ".$row['userid'].", '".$subj."', '".$_POST['text']."', ".time().")");
			$db->query("UPDATE `users` SET `new_mail` = `new_mail` + 1 WHERE `userid` = ".$row['userid']);
			++$cnt;
		}
		$mtg->success("Your message has been sent to all ".$mtg->format($cnt)." members");
	} else {
		?><form action='yourgang.php?action=staff&amp;act2=massmailer' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Subject</th>
					<td width='75%'><input type='text' name='subj' placeholder='This is optional' /></td>
				</tr>
				<tr>
					<th>Message</th>
					<td><textarea name='text' rows='10' cols='70'></textarea></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' value='Send' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_staff_masspayment() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs;
	$_POST['amt'] = isset($_POST['amt']) && ctype_digit(str_replace(',', '', $_POST['amt'])) ? str_replace(',', '', $_POST['amt']) : null;
	$select = $db->query("SELECT `userid` FROM `users` WHERE `gang` = ".$gangdata['gangID']);
	if(!empty($_POST['amt'])) {
		$paidIDs = [];
		$unpaidIDs = [];
		while($r = $db->fetch_row($q)) {
			if($gangdata['gangMONEY'] >= $_POST['amt']) {
				$paidIDs[] = $r['userid'];
				$db->query("UPDATE `users` SET `money` = `money` + ".$_POST['amt']." WHERE `userid` = ".$r['userid']);
				event_add($r['userid'], "You were paid ".money_formatter($_POST['amt'])." by your gang.");
				$gangdata['gangMONEY'] -= $_POST['amt'];
			} else
				$unpaidIDs[] = $r['userid']);
		}
		$paidCount = count($paidIDs);
		$unpaidCount = count($unpaidIDs);
		$paid = '';
		$unpaid = '';
		foreach($paidIDs as $id)
			$paid .= ', '.$mtg->username($id);
		foreach($unpaidIDs as $id)
			$unpaid .= ', '.$mtg->username($id);
		$db->query("UPDATE `gangs` SET `gangMONEY` = GREATEST(0, ".$gangdata['gangMONEY'].") WHERE `gangID` = ".$gangdata['gangID']);
		$gangs->gang_event_add($gangdata['gangID'], "A mass payment was sent. ".$mtg->format($paidCount)."/".$mtg->format($gangs->memberCount())." members were paid");
		?><strong>Members Paid (<?php echo $mtg->format($paidCount);?>):</strong> <?php echo $paidCount ? substr($paid, 0, -2) : 'No-one';?><br />
		?><strong>Members Not Paid (<?php echo $mtg->format($unpaidCount);?>):</strong> <?php echo $unpaidCount ? substr($unpaid, 0, -2) : 'No-one';
	} else {
		if(!$gangdata['gangMONEY'])
			$mtg->error("Your vault doesn't have any cash");
		$maxFair = floor($gangdata['gangMONEY'] / $gangs->memberCount());
		?><form action='yourgang.php?action=staff&amp;act2=masspayment' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Cash Per Player</th>
					<td width='75%'><input type='text' name='amt' value='<?php echo $mtg->format($maxFair);?>' /></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='submit' value='Payout' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_staff_desc() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs, $set;
	if(!$gangs->isLeader(true))
		$mtg->error("You don't have permission to be here");
	if(isset($_POST['subm'])) {
		$_POST['desc'] = isset($_POST['desc']) && is_string($_POST['desc']) ? $db->escape($_POST['desc']) : null;
		if(isset($set['gang_min_char_req_description']) && $set['gang_min_char_req_description'])
			if($set['gang_min_char_req_description'] > strlen($_POST['desc']))
				$mtg->error("Your description requires a minimum of ".$mtg>format($set['gang_min_char_req_description'])." character".$mtg->s($set['gang_min_char_req_description']));
		if(isset($set['gang_max_char_req_description']) && $set['gang_max_char_req_description'])
			if(strlen($_POST['desc']) > $set['gang_max_char_req_description'])
				$mtg->error("Your description is longer than the maximum of ".$mtg>format($set['gang_max_char_req_description'])." character".$mtg->s($set['gang_max_char_req_description']));
		$db->query("UPDATE `gangs` SET `gangDESC` = '".$_POST['desc']."' WHERE `gangID` = ".$gangdata['gangID']);
		$gangs->gang_event_add($gangdata['gangID'], "The description has been updated");
		$mtg->success("You've updated the description");
	} else {
		?><form action='yourgang.php?action=staff&amp;act2=desc' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Current</th>
					<td width='75%'><textarea rows='10' cols='70' disabled='disabled'><?php echo $mtg->format($gangdata['gangDESC']);?></textarea></td>
				</tr>
				<tr>
					<th>New</th>
					<td><textarea name='desc' rows='10' cols='70'></textarea></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='subm' value='Update Description' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_staff_ament() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs, $set;
	if(!$gangs->isLeader(true))
		$mtg->error("You don't have permission to be here");
	if(isset($_POST['subm'])) {
		$_POST['ament'] = isset($_POST['ament']) && is_string($_POST['ament']) ? $db->escape($_POST['ament']) : null;
		if(isset($set['gang_min_char_req_announcement']) && $set['gang_min_char_req_announcement'])
			if($set['gang_min_char_req_announcement'] > strlen($_POST['ament']))
				$mtg->error("Your announcement requires a minimum of ".$mtg>format($set['gang_min_char_req_announcement'])." character".$mtg->s($set['gang_min_char_req_announcement']));
		if(isset($set['gang_max_char_req_announcement']) && $set['gang_max_char_req_announcement'])
			if($set['gang_max_char_req_announcement'] > strlen($_POST['ament']))
				$mtg->error("Your announcement is longer than the maximum of ".$mtg>format($set['gang_max_char_req_announcement'])." character".$mtg->s($set['gang_max_char_req_announcement']));
		$db->query("UPDATE `gangs` SET `gangAMENT` = '".$_POST['amnt']."' WHERE `gangID` = ".$gangdata['gangID']);
		$gangs->gang_event_add($gangdata['gangID'], "The announcement has been updated");
		$mtg->success("You've updated the gang announcement");
	} else {
		?><form action='yourgang.php?action=staff&amp;act2=ament' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Current</th>
					<td width='75%'><textarea rows='10' cols='70' disabled='disabled'><?php echo $mtg->format($gangdata['gangAMENT']);?></textarea></td>
				</tr>
				<tr>
					<th>New</th>
					<td><textarea name='ament' rows='10' cols='70'></textarea></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='subm' value='Update Announcement' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_staff_name() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs, $set;
	if(!$gangs->isLeader())
		$mtg->error("You don't have permission to be here");
	if(isset($_POST['subm'])) {
		$_POST['name'] = isset($_POST['name']) && is_string($_POST['name']) ? $db->escape($_POST['name']) : null;
		if(isset($set['gang_min_char_req_name']) && $set['gang_min_char_req_name'])
			if($set['gang_min_char_req_name'] > strlen($_POST['name']))
				$mtg->error("Your name requires a minimum of ".$mtg>format($set['gang_min_char_req_name'])." character".$mtg->s($set['gang_min_char_req_name']));
		if(isset($set['gang_max_char_req_name']) && $set['gang_max_char_req_name'])
			if(strlen($_POST['name']) > $set['gang_max_char_req_name'])
				$mtg->error("Your name is longer than the maximum of ".$mtg>format($set['gang_max_char_req_name'])." character".$mtg->s($set['gang_max_char_req_name']));
		$db->query("UPDATE `gangs` SET `gangNAME` = '".$_POST['name']."' WHERE `gangID` = ".$gangdata['gangID']);
		$gangs->gang_event_add($gangdata['gangID'], "The name has been updated");
		$mtg->success("You've updated the gang's name");
	} else {
		?><form action='yourgang.php?action=staff&amp;act2=name' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Current</th>
					<td width='75%'><?php echo $mtg->format($gangdata['gangNAME']);?></td>
				</tr>
				<tr>
					<th>New</th>
					<td><input type='text' name='name' /></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='subm' value='Update Announcement' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_staff_tag() {
	global $db, $ir, $userid, $gangdata, $mtg, $gangs, $set;
	if(!$gangs->isLeader())
		$mtg->error("You don't have permission to be here");
	if(isset($_POST['subm'])) {
		$_POST['tag'] = isset($_POST['tag']) && is_string($_POST['tag']) ? $db->escape($_POST['tag']) : null;
		if(isset($set['gang_min_char_req_tag']) && $set['gang_min_char_req_tag'])
			if($set['gang_min_char_req_tag'] > strlen($_POST['tag']))
				$mtg->error("Your tag requires a minimum of ".$mtg>format($set['gang_min_char_req_tag'])." character".$mtg->s($set['gang_min_char_req_tag']));
		if(isset($set['gang_max_char_req_tag']) && $set['gang_max_char_req_tag'])
			if(strlen($_POST['tag']) > $set['gang_max_char_req_tag'])
				$mtg->error("Your tag is longer than the maximum of ".$mtg>format($set['gang_max_char_req_tag'])." character".$mtg->s($set['gang_max_char_req_tag']));
		$db->query("UPDATE `gangs` SET `gangPREF` = '".$_POST['tag']."' WHERE `gangID` = ".$gangdata['gangID']);
		$gangs->gang_event_add($gangdata['gangID'], "The tag has been updated");
		$mtg->success("You've updated the gang's tag");
	} else {
		?><form action='yourgang.php?action=staff&amp;act2=tag' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Current</th>
					<td width='75%'><?php echo $mtg->format($gangdata['gangPREF']);?></td>
				</tr>
				<tr>
					<th>New</th>
					<td><input type='text' name='tag' /></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='subm' value='Update Tag' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_staff_banner() {
	global $ir, $userid, $gangdata, $mtg, $gangs, $set;
	if(!$gangs->isLeader())
		$mtg->error("You don't have permission to be here");
	if(isset($_POST['subm'])) {
		$_POST['banner'] = isset($_POST['banner']) && is_string($_POST['banner']) ? urldecode($_POST['banner']) : null;
		if(empty($_POST['banner']))
			$mtg->error("You didn't enter a valid image path");
		$banner = @getimagesize($_POST['banner']);
		if(!is_array($image) || !$banner[0] || !$banner[1])
			$mtg->error("That's not a valid image path");
		$db->query("UPDATE `gangs` SET `gangBANNER` = '".$_POST['banner']."' WHERE `gangID` = ".$gangdata['gangID']);
		$gangs->gang_event_add($gangdata['gangID'], "The banner has been updated");
		$mtg->success("You've updated the gang's banner");
	} else {
		?><form action='yourgang.php?action=staff&amp;act2=banner' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Current</th>
					<td width='75%'><?php echo $mtg->format($gangdata['gangPREF']);?></td>
				</tr>
				<tr>
					<th>New</th>
					<td><input type='text' name='banner' /></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='subm' value='Update Banner' /></td>
				</tr>
			</table>
		</form><?php
	}
}
function gang_view_armoury() {
	global $set, $db, $ir, $mtg,$userid, $gangdata, $mtg, $gangs;
	if($gangdata['armoury_is_closed'] == 'No')
		$mtg->error("The armoury is currently closed");
	if($gangdata['gang_armoury_item_donation_enabled'] == 'Yes')
		echo "<hr width='75%'><a href='yourgang.php?action=donateitem' style='font-weight:700;'>Donate an Item</a>";
	if($gangs->isLeader())
		echo " &middot; <a href='yourgang.php?action=staff&amp;act2=takeall'>Take All Items</a> &middot; <a href='yourgang.php?action=staff&amp;act2=trashall'>Trash All Items</a><br />";
	$selectItems = $db->query("SELECT `g`.`item`, `g`.`qty`, `g`.`total`, `i`.`itmname` "  .
		"FROM `gang_armoury` AS `g` " .
		"LEFT JOIN `items` AS `i` ON (`g`.`item` = `i`.`itmid`) " .
		"WHERE `g`.`gang` = ".$gangdata['gangID']);
	?><table class='table center' width='75%'>
		<tr>
			<th width='33%'>Item</th>
			<th width='34%'>Quantity</th>
			<th width='33%'>Links</th>
		</tr><?php
		if(!$db->num_rows($selectItems))
			echo "<tr><td colspan='3' class='center'>There are no items available in the Armoury</td></tr>";
		else
			while($row = $db->fetch_row($selectItems)) {
				$selectInventory = $db->query("SELECT `inv_borrowed` FROM `inventory` WHERE `inv_itemid` = ".$row['item']);
				?><tr>
					<td><?php echo $mtg->format($row['itmname']);?></td>
					<td><?php echo $mtg->format($row['qty']), '/', $mtg->format($row['total']);?></td>
					<td style='text-align:center;'><?php
						echo $db->num_rows($selectInventory) && $db->fetch_single($selectInventory) == 'Yes'
							? "<span style='color:#888;'>Borrowed</span>" :
								($gangdata['gang_armoury_item_withdrawable'] == 'Yes')
									? "<a href='yourgang.php?action=borrowitem&ID=".$row['item']."'>Borrow</a>"
									: "<span style='color:#888;'>Locked</span>";
						echo $gangs->isLeader() ? " &middot; <a href='yourgang.php?action=staff&amp;act2=trash&amp;ID=".$row['item']."'>Trash</a>" : '';
					?></td>
				</tr><?php
			}
	?></table><?php
}
function gang_borrow_item() {
	global $set, $db, $ir, $mtg, $gangs, $gangdata, $mtg;
	if($gangdata['armoury_is_closed'] == 'No')
		$mtg->error("The armoury is currently closed");
	if($gangdata['gang_armoury_item_withdrawable'] == 'No')
		$mtg->error("Your Gang has locked the Armoury from being withdrawable");
	$_GET['ID'] = isset($_GET['ID']) && ctype_digit($_GET['ID']) ? $_GET['ID'] : null;
	if(empty($_GET['ID']))
		$mtg->error("You didn't specify a valid item");
	$selectItem = $db->query("SELECT `g`.`item`, `g`.`qty`, `i`.`itmname` " .
		"FROM `gang_armoury` AS `g` ".
		"LEFT JOIN `items` AS `i` ON (`g`.`item` = `i`.`itmid`) ".
		"WHERE `g`.`gang` = ".$gangdata['gangID']." AND `g`.`item` = ".$_GET['ID']);
	if(!$db->num_rows($selectItem))
		$mtg->error("Your Gang doesn't own that item");
	$item = $db->fetch_row($selectItem);
	if(!$item['qty'])
		$mtg->error("Your Gang doesn't have any of that item in stock");
	$selectInventory = $db->query("SELECT `inv_borrowed` FROM `inventory` WHERE `inv_itemid` = ".$_GET['ID']." AND `inv_userid` = ".$ir['userid']);
	if($db->num_rows($selectInventory))
		if($db->fetch_single($selectInventory) == 'Yes')
			$mtg->error("You're already borrowing this item.");
	$db->query("INSERT INTO `inventory` (`inv_itemid`, `inv_userid`, `inv_qty`, `inv_borrowed`, `inv_time`) VALUES (".$_GET['ID'].", ".$ir['userid'].", 1, 'Yes', ".time().")");
	$db->query("INSERT INTO `gang_armoury_loans` (`gang`, `userid`, `item`) VALUES (".$gangdata['gangID'].", ".$ir['userid'].", ".$_GET['ID'].")");
	$db->query("UPDATE `gang_armoury` SET `qty` = `qty` - 1 WHERE `item` = ".$_GET['ID']." AND `gang` = ".$gangdata['gangID']);
	$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." borrowed the ".$mtg->format($item['itmname'])." from the Gang Armoury");
	$mtg->success("You've borrowed the ".$mtg->format($item['itmname'])." from the Gang Armoury, be sure to return it");
}
function gang_donate_item() {
	global $set, $db, $ir, $mtg, $gangs, $gangdata;
	if($gangdata['gang_armoury_item_donation_enabled'] == 'No')
		$mtg->error("Donating items to the Gang Armoury has been temporarily blocked");
	if(isset($_POST['submit'])) {
		$_POST['item'] = isset($_POST['item']) && ctype_digit($_POST['item']) ? $_POST['item'] : null;
		$_POST['qty']  = isset($_POST['qty']) && ctype_digit(str_replace(',', '', $_POST['qty'])) ? str_replace(',', '', $_POST['qty']) : null;
		if(empty($_POST['item']))
			$mtg->error("You didn't select a valid item");
		if(empty($_POST['qty']))
			$mtg->error("You didn't enter a valid quantity");
		$selectItem = $db->query("SELECT `inv`.`inv_qty`, `i`.`itmname` " .
			"FROM `inventory` AS `inv` " .
			"LEFT JOIN `items` AS `i` ON (`inv`.`inv_itemid` = `i`.`itmid`) " .
			"WHERE `inv`.`inv_itemid` = ".$_POST['item']." AND `inv`.`inv_userid` = ".$ir['userid']);
		if(!$db->num_rows($selectItem))
			$mtg->error("You don't own that item");
		$item       = $db->fetch_row($selectItem);
		if($_POST['qty'] > $item['inv_qty'])
			$mtg->error("You don't have enough ".$mtg->format($item['itmname']).$mtg->s($_POST['qty'], $item['itmname'])." to deposit that many");
		item_remove($ir['userid'], $_POST['item'], $_POST['qty']);
		$selectExisting = $db->query("SELECT `total` FROM `gang_armoury` WHERE `gang` = ".$gangdata['gangID']." AND `item` = ".$_POST['item']);
		if(!$db->num_rows($selectExisting))
			$db->query("INSERT INTO `gang_armoury` (`gang`, `item`, `qty`, `total`) VALUES (".$gangdata['gangID'].", ".$_POST['item'].", ".$_POST['qty'].", ".$_POST['qty'].")");
		else
			$db->query("UPDATE `gang_armoury` SET `qty` = `qty` + ".$_POST['qty'].", `total` = `total` + ".$_POST['qty']." WHERE `gang` = ".$gangdata['gangID']." AND `item` = ".$_POST['item']);
		$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." Donated ".$mtg->format($_POST['qty'])." ".$mtg->format($item['itmname']).($_POST['qty'] == 1 ? '' : $itemPlural)." to the Gang Armoury");
		?><hr width='75%'>You haveve donated <?php echo $mtg->format($_POST['qty']), ' ', $mtg->format($item['itmname']), ($_POST['qty'] == 1 ? '' : $itemPlural);?> to your Gang Armoury<?php
	}
	$selectInventory = $db->query("SELECT `inv`.`inv_itemid`, `inv`.`inv_qty`, `i`.`itmname` FROM `inventory` AS `inv` "."LEFT JOIN `items` AS `i` ON (`inv`.`inv_itemid` = `i`.`itmid`) "."WHERE `inv`.`inv_userid` = ".$ir['userid']);
	if(!$db->num_rows($selectInventory))
		$mtg->error("You have no items to donate");
	?><form action='yourgang.php?action=donateitem' method='post'>
		<table class='table'>
			<tr>
				<th width='25%'>Item</th>
				<td width='75%'><select name='item' width='74%'><?php
					while($row = $db->fetch_row($selectInventory))
						printf("<option value='%u'>%s [x%s]</option>", $row['inv_itemid'], $mtg->format($row['itmname']), $mtg->format($row['inv_qty']));
				?></select></td>
			</tr>
			<tr>
				<th>Quantity</th>
				<td><input type='number' name='qty' value='1' width='74%' /></td>
			</tr>
			<tr>
				<td colspan='2' class='center'><input type='submit' name='submit' value='Donate' /></td>
			</tr>
		</table>
	</form><?php
}
function gang_staff_edit_armoury_settings() {
	global $set, $db, $ir, $mtg, $gangs, $gangdata;
	if(!$gangs->isLeader())
		$mtg->error("You don't have permission to be here");
	if(!isset($_POST['submit'])) {
		$settings = [
			'Access Armoury' => 'armoury_is_closed',
			'Withdrawable' => 'gang_armoury_item_withdrawable',
			'Donations Enabled' => 'gang_armoury_item_donation_enabled',
			'Automatically returned' => 'gang_armoury_item_auto_returned'
		];
		?><form action='yourgang.php?action=staff&amp;act2=editarmoury' method='post'>
			<hr /><table class='table' width='75%'><?php
				foreach($settings as $desc => $name) {
					?><tr>
						<th width='35%'><?php echo $desc;?></th>
						<td width='65%'><select name='<?php echo $name;?>'>
							<option value='Yes'<?php echo $gangdata[$name] == 'Yes' ? " selected='selected'" : '';?>>Yes</option>
							<option value='No'<?php echo $gangdata[$name] == 'No' ? " selected='selected'" : '';?>>No</option>
						</select></td>
					</tr><?php
				}
				?><tr>
					<th>Returns: Timeframe (in days)</th>
					<td><input type='number' name='gang_armoury_item_auto_returned_time_frame' value='<?php echo $gangdata['gang_armoury_item_auto_returned_time_frame'];?>' /></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='submit' value='Submit Changes' /></td>
				</tr>
			</table>
		</form><?php
	} else {
		$required = [
			'armoury_is_closed',
			'gang_armoury_item_withdrawable',
			'gang_armoury_item_donation_enabled',
			'gang_armoury_item_auto_returned'
		];
		foreach($required as $name)
			if(empty($_POST[$name]))
				$mtg->error("You missed something.. ".ucwords(str_replace('_', ' ', $name)));
		if(empty($_POST['gang_armoury_item_auto_returned_time_frame']))
			$mtg->error("You missed something.. auto return time");
		foreach($required as $choice)
			if(!in_array($_POST[$choice], ['Yes', 'No']))
				$mtg->error("You didn't select a valid choice");
		$db->query("REPLACE INTO `gang_settings` (`gangid`, `armoury_is_closed`, `gang_armoury_item_withdrawable`, `gang_armoury_item_donation_enabled`, `gang_armoury_item_auto_returned`, `gang_armoury_item_auto_returned_time_frame`) VALUES (".$gangdata['gangID'].", '".$_POST['armoury_is_closed']."', '".$_POST['gang_armoury_item_withdrawable']."', '".$_POST['gang_armoury_item_donation_enabled']."', '".$_POST['gang_armoury_item_auto_returned']."', ".$_POST['gang_armoury_item_auto_returned_time_frame'].")");
		$mtg->success("You've updated your Gang Armoury's settings");
	}
}
function gang_staff_armoury_trash_item() {
	global $set, $db, $ir, $mtg, $gangs, $gangdata;
	if(!$gangs->isLeader())
		$mtg->error("You don't have permission to be here");
	$_GET['ID'] = isset($_GET['ID']) && ctype_digit($_GET['ID']) ? $_GET['ID'] : null;
	if(empty($_GET['ID']))
		$mtg->error("You didn't select a valid item");
	$selectItem = $db->query("SELECT `g`.`item`, `g`.`qty`, `g`.`total`, `i`.`itmname` " .
		"FROM `gang_armoury` AS `g` " .
		"LEFT JOIN `items` AS `i` ON (`g`.`item` = `i`.`itmid`) " .
		"WHERE `g`.`item` = ".$_GET['ID']." AND `g`.`gang` = ".$gangdata['gangID']);
	if(!$db->num_rows($selectItem))
		$mtg->error("Your Gang doesn't own that item");
	$item = $db->fetch_row($selectItem);
	if($item['qty'] != $item['total'])
		$mtg->error(($item['total'] - $item['qty'])." member".$mtg->s($item['total'] - $item['qty'])." of your Gang ha".(($item['total'] - $item['qty']) == 1 ? 's' : 've')." borrowed the ".$mtg->format($item['itmname']).". You can't trash it until they've all been returned");
	$db->query("DELETE FROM `gang_armoury` WHERE `gang` = ".$gangdata['gangID']." AND `item` = ".$_GET['ID']);
	$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." removed the ".$mtg->format($item['itmname']).$mtg->s($item['total'], $item['itmname'])." from the Gang Armoury");
	$mtg->success("You've removed the ".$mtg->format($item['itmname']).$mtg->s($item['total'], $item['itmname'])." from your Gang Armoury");
	gang_view_armoury();
}
function gang_staff_armoury_trash_all_items() {
	global $set, $db, $ir, $mtg, $gangs, $gangdata;
	if(!$gangs->isLeader())
		$mtg->error("You don't have permission to be here");
	$selectItems = $db->query("SELECT `g`.`item`, `g`.`qty`, `g`.`total`, `i`.`itmname` " .
		"FROM `gang_armoury` AS `g` " .
		"LEFT JOIN `items` AS `i` ON (`g`.`item` = `i`.`itmid`) " .
		"WHERE `g`.`gang` = ".$gangdata['gangID']);
	if(!$db->num_rows($selectItems))
		$mtg->error("Your Gang doesn't have any items");
	while($item = $db->fetch_row($selectItems))
		if($item['qty'] != $item['total'])
			$mtg->error(($item['total'] - $item['qty'])." member".$mtg->s($item['total'] - $item['qty'])." of your Gang ha".(($item['total'] - $item['qty']) == 1 ? 's' : 've')." borrowed the ".$mtg->format($item['itmname']).". You can't trash anything until they've all been returned");
	$db->query("DELETE FROM `gang_armoury` WHERE `gang` = ".$gangdata['gangID']);
	$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." removed all the items from the Gang Armoury");
	$mtg->success("You've removed all items from your Gang Armoury");
	gang_view_armoury();
}
function gang_staff_armoury_take_all_items() {
	global $set, $db, $ir, $mtg, $gangs, $gangdata;
	if(!$gangs->isLeader())
		$mtg->error("You don't have permission to be here");
	$all         = true;
	$selectItems = $db->query("SELECT `g`.`item`, `g`.`qty`, `g`.`total`, `i`.`itmname` " .
		"FROM `gang_armoury` AS `g` " .
		"LEFT JOIN `items` AS `i` ON (`g`.`item` = `i`.`itmid`) " .
		"WHERE `g`.`gang` = ".$gangdata['gangID']);
	if(!$db->num_rows($selectItems))
		$mtg->error("Your Gang doesn't have any items");
	else
		while($item = $db->fetch_row($selectItems)) {
			if($item['qty'] == $item['total']) {
				item_add($ir['userid'], $item['item'], $item['total']);
				$db->query("DELETE FROM `gang_armoury` WHERE `gang` = ".$gangdata['gangID']." AND `item` = ".$item['item']);
			} else {
				$all = false;
				$db->query("UPDATE `gang_armoury` SET `total` = `total` - `qty`, `qty` = 0 WHERE `qty` > 0 AND `gang` = ".$gangdata['gangID']." AND `item` = ".$item['item']);
				item_add($ir['userid'], $item['item'], $item['qty']);
			}
		}
	$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." took all the items from the Gang Armoury");
	$mtg->success("You've taken all items from your Gang Armoury.".(!$all ? " There are still some items that need to be returned from your members before you can take them" : ''));
	gang_view_armoury();
}
function gang_staff_armoury_recall_item() {
	global $set, $db, $ir, $mtg, $gangs, $gangdata;
	if(!$gangs->isLeader())
		$mtg->error("You don't have permission to be here");
	$_GET['ID']   = isset($_GET['ID']) && ctype_digit($_GET['ID']) ? $_GET['ID'] : null;
	$_GET['user'] = isset($_GET['user']) && ctype_digit($_GET['user']) ? $_GET['user'] : null;
	if(!empty($_GET['ID']) && !empty($_GET['user'])) {
		$selectItem = $db->query("SELECT `item` FROM `gang_armoury` WHERE `gang` = ".$gangdata['gangID']." AND `item` = ".$_GET['ID']);
		if(!$db->num_rows($selectItem))
			$mtg->error("Your Gang doesn't own this item");
		$selectLoan = $db->query("SELECT `g`.`userid`, `g`.`item`, `i`.`itmname` " .
			"FROM `gang_armoury_loans` AS `g` " .
			"LEFT JOIN `items` AS `i` ON (`g`.`item` = `i`.`itmid`) " .
			"WHERE `g`.`gang` = ".$gangdata['gangID']." AND `g`.`item` = ".$_GET['ID']);
		if(!$db->num_rows($selectLoan))
			$mtg->error("That item hasn't been loaned out");
		$item            = $db->fetch_row($selectLoan);
		$selectInventory = $db->query("SELECT `inv_borrowed` FROM `inventory` WHERE `inv_userid` = ".$item['userid']." AND `inv_itemid` = ".$_GET['ID']);
		if(!$db->num_rows($selectInventory)) {
			$selectEquipment = $db->query("SELECT `equip_primary`, `equip_secondary`, `equip_armor` FROM `users` WHERE `equip_primary` = ".$_GET['ID']." OR `equip_secondary` = ".$_GET['ID']." OR `equip_armor` = ".$_GET['ID']." AND `userid` = ".$item['userid']);
			if(!$db->num_rows($selectEquipment))
				$mtg->error("The ".$mtg->format($item['itmname'])." can't be found. ".$mtg->username($item['userid'])." hasn't equipped it, nor is it in their inventory..");
			$equipped = $db->fetch_row($selectEquipment);
			if($equipped['equip_primary'] == $_GET['ID'])
				$db->query("UPDATE `users` SET `equip_primary` = 0 WHERE `userid` = ".$item['userid']);
			else if($equipped['equip_secondary'] == $_GET['ID'])
				$db->query("UPDATE `users` SET `equip_secondary` = 0 WHERE `userid` = ".$item['userid']);
			if($equipped['equip_armor'] == $_GET['ID'])
				$db->query("UPDATE `users` SET `equip_armor` = 0 WHERE `userid` = ".$item['userid']);
		}
		$db->query("DELETE FROM `gang_armoury_loans` WHERE `gang` = ".$gangdata['gangID']." AND `item` = ".$_GET['ID']." AND `userid` = ".$_GET['user']);
		$db->query("UPDATE `gang_armoury` SET `qty` = `qty` + 1 WHERE `gang` = ".$gangdata['gangID']." AND `item` = ".$_GET['ID']);
		item_remove($_GET['user'], $_GET['ID'], 1, 1);
		event_add($_GET['user'], "Your Gang has recalled the ".$mtg->format($item['itmname'])." that you borrowed");
		$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." recalled the ".$mtg->format($item['itmname'])." from ".$mtg->username($item['userid']));
		$mtg->success("You've recalled the ".$mtg->format($item['itmname'])." from ".$mtg->username($item['userid']));
	}
	$selectLoanedItems = $db->query("SELECT `g`.`userid`, `g`.`item`, `i`.`itmname` " .
		"FROM `gang_armoury_loans` AS `g` " .
		"LEFT JOIN `items` AS `i` ON (`g`.`item` = `i`.`itmid`) " .
		"WHERE `g`.`gang` = ".$gangdata['gangID']);
	if(!$db->num_rows($selectLoanedItems))
		$mtg->error("Your members haven't borrowed any items");
	?><table class='table' width='75%'>
		<tr>
			<th width='45%'>Member</th>
			<th width='45%'>Item</th>
			<th width='10%'>Links</th>
		</tr><?php
			while($row = $db->fetch_row($selectLoanedItems)) {
				?><tr>
					<td><?php echo $mtg->username($row['userid'], true);?></td>
					<td><a href='iteminfo.php?ID=<?php echo $row['item'];?>'><?php echo $mtg->format($row['itmname']);?></a></td>
					<td><a href='yourgang.php?action=staff&amp;act2=recall&amp;ID=<?php echo $row['item'];?>&amp;user=<?php echo $row['userid'];?>'>Recall</a></td>
				</tr><?php
			}
	?></table><?php
}
function gang_staff_armoury_leader_take_item() {
	global $set, $db, $ir, $mtg, $gangs, $gangdata;
	if(!$gangs->isLeader())
		$mtg->error("You don't have permission to be here");
	$_GET['ID'] = isset($_GET['ID']) && ctype_digit($_GET['ID']) ? $_GET['ID'] : null;
	$selectItem = $db->query("SELECT `g`.`item`, `g`.`qty`, `g`.`total`, `i`.`itmname` " .
		"FROM `gang_armoury` AS `g` " .
		"LEFT JOIN `items` AS `i` ON (`g`.`item` = `i`.`itmid`) " .
		"WHERE `g`.`gang` = ".$gangdata['gangID']." AND `g`.`item` = ".$_GET['ID']);
	if(!$db->num_rows($selectItem))
		$mtg->error("Your Gang doesn't own this item");
	$row = $db->fetch_row($selectItem);
	if(!$row['qty'])
		$mtg->error("You must recall that item before you can take it from your Gang");
	if($row['total'] == 1)
		$db->query("DELETE FROM `gang_armoury` WHERE `item` = ".$_GET['ID']." AND `gang` = ".$gangdata['gangID']);
	else
		$db->query("UPDATE `gang_armoury` SET `total` = `total` - 1 WHERE `item` = ".$_GET['ID']." AND `gang` = ".$gangdata['gangID']);
	item_add($ir['userid'], $_GET['ID'], 1);
	$gangs->gang_event_add($gangdata['gangID'], $mtg->username($ir['userid'])." took the ".$mtg->format($row['itmname'])." from the gang armoury");
	$mtg->success("You have taken the ".$mtg->format($row['itmname'])." from your Gang Armoury");
}
$h->endpage();