INSERT INTO `settings` (`conf_name`, `conf_value`) VALUES
	('staff_colour_enabled', 0),
	('staff_colour_admin', '003EFF'),
	('staff_colour_secretary', '551A8B'),
	('staff_colour_assistant', 'CD6600'),
	('gang_oc_cancel_penalty', 1),
	('gang_min_char_req_mail', 0),
	('gang_max_char_req_mail', 0),
	('gang_min_char_req_description', 0),
	('gang_max_char_req_description', 0),
	('gang_min_char_req_announcement', 0),
	('gang_max_char_req_announcement', 0),
	('gang_min_char_req_name', 4),
	('gang_max_char_req_name', 4),
	('gang_min_char_req_tag', 3),
	('gang_max_char_req_tag', 3)
;
CREATE TABLE IF NOT EXISTS `gang_armoury` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`gang` INT(11) NOT NULL,
	`item` INT(11) NOT NULL,
	`qty` INT(11) NOT NULL,
	`total` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `gang_armoury_loans` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`gang` INT(11) NOT NULL,
	`userid` INT(11) NOT NULL,
	`item` INT(11) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `gang_settings` (
	`gangid` INT(11) NOT NULL,
	`armoury_is_closed` ENUM('Yes','No') NOT NULL DEFAULT 'No',
	`gang_armoury_item_withdrawable` ENUM('Yes','No') NOT NULL DEFAULT 'No',
	`gang_armoury_item_auto_returned` ENUM('Yes','No') NOT NULL DEFAULT 'No',
	`gang_armoury_item_auto_returned_time_frame` INT(11) NOT NULL DEFAULT '0',
	`gang_armoury_item_donation_enabled` ENUM('Yes','No') NOT NULL DEFAULT 'Yes',
	PRIMARY KEY (`gangid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `gangs` (
	`gangID` INT(11) NOT NULL AUTO_INCREMENT,
	`gangNAME` VARCHAR(255) NOT NULL DEFAULT '',
	`gangDESC` TEXT NOT NULL,
	`gangPREF` VARCHAR(12) NOT NULL DEFAULT '',
	`gangSUFF` VARCHAR(12) NOT NULL DEFAULT '',
	`gangMONEY` INT(11) NOT NULL DEFAULT '0',
	`gangRESPECT` INT(11) NOT NULL DEFAULT '0',
	`gangPRESIDENT` INT(11) NOT NULL DEFAULT '0',
	`gangVICEPRES` INT(11) NOT NULL DEFAULT '0',
	`gangCAPACITY` INT(11) NOT NULL DEFAULT '0',
	`gangCRIME` INT(11) NOT NULL DEFAULT '0',
	`gangCHOURS` INT(11) NOT NULL DEFAULT '0',
	`gangAMENT` LONGTEXT NOT NULL,
	`gangBANNER` VARCHAR(255) NOT NULL DEFAULT '',
	`gangWAR` ENUM('Peaceful','Aggressive') NOT NULL DEFAULT 'Peaceful',
PRIMARY KEY (`gangID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;