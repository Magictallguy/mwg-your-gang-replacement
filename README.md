# This has been tested for __parse errors only__.
### I no longer use the MC Craps engine so I have no place to test (nor do I have a copy of MC Craps..)

#Your Gang Replacement

### Database updates
Import and run yourgang.sql

#### Important!
If you *do* already have a `gangs` table in your database, run the query below.
```SQL
ALTER TABLE `gangs`
	ADD `gangBANNER` VARCHAR( 255 ) NOT NULL DEFAULT '',
	ADD `gangWAR` ENUM('Peaceful','Aggressive') NOT NULL DEFAULT 'Peaceful'
;
```
If you *don't* have the `gangs` table, run this query
```SQL
CREATE TABLE `gangs` (
	`gangID` INT(11) NOT NULL AUTO_INCREMENT,
	`gangNAME` VARCHAR(255) NOT NULL DEFAULT '',
	`gangDESC` TEXT NOT NULL,
	`gangPREF` VARCHAR(12) NOT NULL DEFAULT '',
	`gangSUFF` VARCHAR(12) NOT NULL DEFAULT '',
	`gangMONEY` INT(11) NOT NULL DEFAULT '0',
	`gangRESPECT` INT(11) NOT NULL DEFAULT '0',
	`gangPRESIDENT` INT(11) NOT NULL DEFAULT '0',
	`gangVICEPRES` INT(11) NOT NULL DEFAULT '0',
	`gangCAPACITY` INT(11) NOT NULL DEFAULT '0',
	`gangCRIME` INT(11) NOT NULL DEFAULT '0',
	`gangCHOURS` INT(11) NOT NULL DEFAULT '0',
	`gangAMENT` LONGTEXT NOT NULL,
	`gangBANNER` VARCHAR(255) NOT NULL DEFAULT '',
	`gangWAR` ENUM('Peaceful','Aggressive') NOT NULL DEFAULT 'Peaceful',
PRIMARY KEY (`gangID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
```
### staff.php - update to the basic settings
Find something like `<input type='submit' value='Submit' />` in the form of the basic settings function.
Add above:
```PHP
";
$enabledOpts = array(
	0 => 'No',
	1 => 'Yes'
);
?><br /><br />
<span style='text-decoration:underlined;font-weight:700;'>Staff Rank Colours</span><br />
<strong>Enable Staff Rank Colours?:</strong> <select name='staff_colour_enabled'><?php
foreach($enabledOpts as $val => $opt)
	printf("<option value='%u'%s>%s</option>", $val, $val == $set['staff_colour_enabled'] ? " selected='selected'" : '', $opt);
?></select><br />
<strong>Administrator Colour (hex code):</strong> <input type='text' name='staff_colour_admin' value='<?php echo $set['staff_colour_admin'];?>' /><br />
<strong>Secretary Colour (hex code):</strong> <input type='text' name='staff_colour_secretary' value='<?php echo $set['staff_colour_secretary'];?>' /><br />
<strong>Assistant Colour (hex code):</strong> <input type='text' name='staff_colour_assistant' value='<?php echo $set['staff_colour_assistant'];?>' /><br /><br />
<span style='text-decoration:underlined;font-weight:700;'>Gang Settings</span><br />
<strong>Organised Crime Cancelation Penalty Enabled?:</strong>  <select name='gang_oc_cancel_penalty'><?php
foreach($enabledOpts as $val => $opt)
	printf("<option value='%u'%s>%s</option>", $val, $val == $set['gang_oc_cancel_penalty'] ? " selected='selected'" : '', $opt);
?></select><br />
<em>*Note: The settings are completely optional. Set to 0 to disable requirements</em><br />
<strong>Minimum Character Count Required (Gang Mass Message):</strong> <input type='text' name='gang_min_char_req_mail' value='<?php echo $set['gang_min_char_req_mail'];?>' /><br />
<strong>Maximum Character Count Required (Gang Mass Message):</strong> <input type='text' name='gang_max_char_req_mail' value='<?php echo $set['gang_max_char_req_mail'];?>' /><br />
<strong>Minimum Character Count Required (Gang Description):</strong> <input type='text' name='gang_min_char_req_description' value='<?php echo $set['gang_min_char_req_description'];?>' /><br />
<strong>Maximum Character Count Required (Gang Description):</strong> <input type='text' name='gang_max_char_req_description' value='<?php echo $set['gang_max_char_req_description'];?>' /><br />
<strong>Minimum Character Count Required (Gang Mass Announcement):</strong> <input type='text' name='gang_min_char_req_announcement' value='<?php echo $set['gang_min_char_req_announcement'];?>' /><br />
<strong>Maximum Character Count Required (Gang Mass Announcement):</strong> <input type='text' name='gang_max_char_req_announcement' value='<?php echo $set['gang_max_char_req_announcement'];?>' /><br />
<strong>Minimum Character Count Required (Gang Name):</strong> <input type='text' name='gang_min_char_req_name' value='<?php echo $set['gang_min_char_req_name'];?>' /><br />
<strong>Maximum Character Count Required (Gang Name):</strong> <input type='text' name='gang_max_char_req_name' value='<?php echo $set['gang_max_char_req_name'];?>' /><br />
<strong>Minimum Character Count Required (Gang Tag):</strong> <input type='text' name='gang_min_char_req_tag' value='<?php echo $set['gang_min_char_req_tag'];?>' /><br />
<strong>Maximum Character Count Required (Gang Tag):</strong> <input type='text' name='gang_max_char_req_tag' value='<?php echo $set['gang_max_char_req_tag'];?>' /><br />
```

### Optional images
Note: If you want these icons, you'll need to create a directory path of `/public_html/img/silk` then upload them

###### Icons from famfamfam - http://famfamfam.com/lab/icons/silk/